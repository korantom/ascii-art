#include "CShader.h"

CShader::CShader() {
    pixelToCharArray.emplace_back( '@', 20 );
    pixelToCharArray.emplace_back( '$', 40 );
    pixelToCharArray.emplace_back( '#', 60 );
    pixelToCharArray.emplace_back( '*', 80 );
    pixelToCharArray.emplace_back( '+', 100 );
    pixelToCharArray.emplace_back( '=', 135 );
    pixelToCharArray.emplace_back( '-', 175 );
    pixelToCharArray.emplace_back( ':', 200 );
    pixelToCharArray.emplace_back( '.', 240 );
    pixelToCharArray.emplace_back( ' ', 251 );
    //todo
    m_size = pixelToCharArray.size();
}

CShader::CShader( const string& filePath ) {

    string line;
    std::ifstream ifs( filePath, std::ios::in );
    if ( ifs ) {
        while ( std::getline( ifs, line ))
            if ( line[0] != '%' )
                pixelToCharArray.emplace_back( line[0], stoi( line.substr( 2, line.length() - 2 )));

        m_size = pixelToCharArray.size();
        sort( pixelToCharArray.begin(), pixelToCharArray.end(), sortByValue );
    } else {
        //todo have a default file somewhere
        pixelToCharArray.emplace_back( '@', 20 );
        pixelToCharArray.emplace_back( '$', 40 );
        pixelToCharArray.emplace_back( '#', 60 );
        pixelToCharArray.emplace_back( '*', 80 );
        pixelToCharArray.emplace_back( '+', 100 );
        pixelToCharArray.emplace_back( '=', 135 );
        pixelToCharArray.emplace_back( '-', 175 );
        pixelToCharArray.emplace_back( ':', 200 );
        pixelToCharArray.emplace_back( '.', 240 );
        pixelToCharArray.emplace_back( ' ', 251 );
        //todo
        m_size = pixelToCharArray.size();
    }
}

CShader::CShader( const CShader& other ) {
    m_size = other.m_size;
    for ( const pair<char, uint8_t>& p : other.pixelToCharArray ) {
        this->pixelToCharArray.emplace_back( p );
    }
}

CShader& CShader::operator=( const CShader& other ) {
    this->pixelToCharArray.clear();
    m_size = other.m_size;

    for ( const pair<char, uint8_t>& p : other.pixelToCharArray ) {
        this->pixelToCharArray.emplace_back( p );
    }
    return *this;
}

void CShader::ExampleSmall( string& str ) {
    str = "";
    for ( size_t i = 0; i < m_size; ++i ) {
        for ( size_t j = 0; j < m_size; ++j ) {
            str += pixelToCharArray[i].first;
        }
    }
}

void CShader::ExampleBig( string& str ) {
    str = "";
    for ( size_t i = 0; i < 255; ++i ) {
        for ( size_t j = 0; j < 255; ++j ) {
            str += this->Match( i );
        }
    }
}

char CShader::Match( const uint8_t& x ) const {
    if ( x <= pixelToCharArray[0].second ) {
        return pixelToCharArray[0].first;
    } else if ( x >= pixelToCharArray[m_size - 1].second ) {
        return pixelToCharArray[m_size - 1].first;
    } else {
        size_t i;
        for ( i = 0; i < m_size; ++i ) {
            if ( x >= pixelToCharArray[i].second && x <= pixelToCharArray[i + 1].second )
//                if ( x >= pixelToCharArray[i].second )//wrong
                break;
        }
        return ((x - pixelToCharArray[i].second) > (pixelToCharArray[i + 1].second - x)) //k cemu se ma bliz
               ? pixelToCharArray[i + 1].first
               : pixelToCharArray[i].first;
    }


}


bool CShader::sortByValue( pair<char, uint8_t>& a, pair<char, uint8_t>& b ) {
    return (a.second < b.second);
}



