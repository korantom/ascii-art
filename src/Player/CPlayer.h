#pragma once

#include <string>
#include <sstream>
#include <ncurses.h>
#include <unistd.h>
#include <dirent.h>
#include <chrono>
#include <vector>

using namespace std;

#define PAIR_BW         1
#define MIN_WIDTH       50
#define MIN_HEIGHT      50

//------------------------------------------------------------------------------------------------------------------------------------
/** Player class creates an gui.
 * allows user play converted ascii images he selected in various playback speeds, pause and unpause, and quit
 */
class CPlayer {
public:
    /** Constructor receives ASCII folder path.
     * initializes player window and other attributes
     * pushes all files in directory into a vector of ascii image paths
     * @param path
     */
    explicit CPlayer( const string& path );

    /** Finite state machine representing stages of playback.
     * (init screen, play, pause,...)
     *
     */
    void Run();

private:
    /** All player States.
     */
    enum EState {
        INIT_MESSAGE,  ///< initial welcome screen informing user of controls etc.
        PLAY,          ///< plays
        PAUSE,         ///< pauses playback
        EXIT,          ///< terminates window and ends player
        DELETE,        ///< coming soon
        NEXT,          ///< coming soon
        PREV,          ///< coming soon
        ERROR          ///< coming soon
    };

    int m_rows;         ///< current number of rows
    int m_cols;         ///< current number of cols
    WINDOW *m_window_menu;  ///< ptr to window object

    EState m_state;        ///< current state
    EState m_nextState;    ///< next state

    bool m_playerOn;       ///< condition in the FSM while cycle

    string m_path;         ///< ASCII files folder path
    vector <string> m_ASCCIFilePaths; ///< vector of individual ASCII files

    const string m_infoMessage = "For Best Results set:\n"
                                 "\tmax terminal size(max height especially)\n"
                                 "\tnon-proportinal font\n"
                                 "\tand/or line spacing to minimum\n"
                                 "\t\n"
                                 "Controls:\n"
                                 "\tto start: left or right arrow\n"
                                 "\tpause:    p\n"
                                 "\tunpause:  s\n"
                                 "\tspeed:    f\n"
                                 "\tquit:     q\n"
                                 "To continue press left/right arrow key\n";

    const double m_playbackSpeedList[6] = {0.05, 0.1, 0.25, 0.5, 1.5, 3.0}; ///< playback speeds
    const int m_numOfSpeeds = 6;                                            ///< pos in m_playbackSpeedList
    int m_playbackSpeed;

    //------------------------------------------------------------------------------------------------------------------------------------------

    /** initializes menu.
     *  sets background and font color
     *  todo ensure minimum size of terminal
     */
    void InitializeMenu();

    /** simple display menu, shows a message in screen and waits for user until he presses an arrow key -> or <-.
     * @param[in] window  pointer to window
     * @param[in] message string to display
     * @param[out] rows   updates number of rows currently on screen
     * @param[out] cols   updates number of cols currently on screen
     */
    void InfoScreen( WINDOW *window, const string& message, int& rows, int& cols );

    /** displays an ascii image and its path centered by width.
     * @param [in] window  pointer to window
     * @param [in] ASCIIFilePath path to ascii txt file
     */
    void display( WINDOW *window, const string& ASCIIFilePath );

    /** sleep for 1 sec.
     */
    inline void delay() {
        sleep( 1 );
    }

    /** infinite playback.
     * reads user input
     * play/pause   's/p'
     * change speed 'f'
     * quit         'q'
     *
     */
    void play();

    /** infinite pause on current image.
     * waits fo user to play or quit
     * play/pause   's/p'
     * quit         'q'
     */
    void pause();

};

