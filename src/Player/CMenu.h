#pragma once

#include <string>
#include <cstring>
#include <sstream>
#include <ncurses.h>
#include <unistd.h>
#include <dirent.h>
#include <vector>

#define PAIR_BW         1 ///< color combination black and white

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------
/** Menu class creates an gui.
 * allows user to simply input images and apply transformations
 * Menu doesnt check if files/directories exist only if they are not empty or contain spaces
 */
class CMenu {
public:
    /** Constructor.
     *
     * @param[out] inputImagePaths vector of paths to all images that are to be displayed later on
     * @param[out] transformationMenu vector of pairs <transformation name, isToBeAppliedInfo>
     *             user using simple checkbox menu chooses which transformations will be applied
     * @param[out] conversionSuccess boolean signaling whether user made made selected any images
     * @param[out] shaderPath path to file with shader data
     */
    CMenu( vector<string> *inputImagePaths, vector<pair<string, bool>> *transformationMenu, bool *conversionSuccess, string *shaderPath );

    //todo
    ~CMenu();

    //------------------------------------------------------------------------------------------------------------------------------------
    /** Finite state machine representing menu.
     *
     */
    void Run();

private:
    /** All menu States.
     */
    enum EState {
        INIT_MESSAGE,    ///< initial welcome screen informing user of controls etc.
        DIRECTORY_PATH, ///< input screen where user can enter directory path
        IMAGE_PATH,     ///< input screen where user can enter image path
        SELECTION,      ///< Main menu
        TRANSFORMATIONS,///< check box menu
        SHADERS,        ///< input screen where user can enter shader file path
        CONVERSION,     ///< terminates menu and saves users input into parameters received in constructor
        EXIT,           ///< terminates window and end menu
        ERROR           ///< displays error messages
    };

    int m_rows;     ///< current number of rows
    int m_cols;     ///< current number of columns
    WINDOW *m_window_menu; ///< ptr to window object

    EState m_state;     ///< current state
    EState m_nextState;///< next state

    bool m_menuOn;      ///< condition in the FSM while cycle

    string m_imageDirectoryPath;
    vector<string> m_imagePaths;

    string& m_shaderPath;


    vector<string>& m_inputImagePaths;              ///< reference to parameter received in constructor
    vector<pair<string, bool>>& m_transformationMenu;///< reference to parameter received in constructor

    //static maybe
    const vector<pair<string, EState>> m_mainMenu = {{"Directory Path",   DIRECTORY_PATH},
                                                     {"Image Path",       IMAGE_PATH},
                                                     {"Shaders",          SHADERS},
                                                     {"Transformations",  TRANSFORMATIONS},
                                                     {"Begin Conversion", CONVERSION},
                                                     {"Exit",             EXIT}};  ///< main menu


    //static
    const string m_infoMessage = "Controls:\n"
                                 "\tForward:    right arrow\n"
                                 "\tBack:       left  arrow\n"
                                 "\tSave Input: Enter key\n"
                                 "\t..\n"
                                 "Only One directory path input accepted\n"
                                 "multiple Image paths paths accepted\n"
                                 "\n"
                                 "To continue press left/right arrow key\n";
    //static
    const string m_errorMessage = "Something went wrong, possible problmes:\n"
                                  "\tIncorrect / Empty Path Name\n"
                                  "\tWrong Terminal size or something else\n"
                                  "\tNo Input provided\n"
                                  "PLEASE Try Again\n"
                                  "..........\n"
                                  "To continue press left/right arrow key\n";


    int m_highlight;            ///< selected item currently
    bool& m_conversionSuccess;   ///< signals whether any image was loaded

//---------------------------------------------------------------------------------------------------------------------------------------
    /** initializes menu.
     *  sets background and font color
     *  todo ensure minimum size of terminal
     */
    void InitializeMenu();

    /** simple display menu, shows a message in screen and waits for user until he presses an arrow key -> or <-.
     * @param[in] window  pointer to window
     * @param[in] message string to display
     * @param[out] rows   updates number of rows currently on screen
     * @param[out] cols   updates number of cols currently on screen
     */
    void InfoScreen( WINDOW *window, const string& message, int& rows, int& cols );

    /** simple tile menu.
     *  allows user to move up and down and either go back to main menu or to the selected item
     * @param[in] window pointer to window
     * @param[out] nextState writes/selects the next state of the FSM in Run method
     * @param[in] back  nextstate if user "goes back"
     * @param[out] menu vector of nextstates user can select if he goes "forward"
     * @param[out] rows  umber of rows currently on screen
     * @param[out] cols number of cols currently on screen
     */
    void TileMenu( WINDOW *window, EState& nextState, EState back, const vector<pair<string, EState>>& menu, int& rows, int& cols );

    /** simple checkbox menu.
     * allows user to check options
     * @param[in] window  pointer to window
     * @param[out] nextState writes/selects the next state of the FSM in Run method
     * @param[in] back
     * @param[out]optinMenu vector of pairs <string option name, bool isSelected>
     * @param[out] rows  number of rows
     * @param[out] cols  number of cols
     */
    void CheckBoxMenu( WINDOW *window, EState& nextState, EState back, vector<pair<string, bool>>& optinMenu, int& rows, int& cols );

    /** simple input menu window.
     * waits untill user provides input (can be empty)
     * @param[in] window  pointer to window
     * @param[out] input variable where the users input is saved
     * @param[in] message message with instructions for user
     */
    void InputMenu( WINDOW *window, string& input, const string& message );

    /** checks if file exists.
     * @param[in] path
     * @return true if file can be opened else false
     */
    static bool fileExists( const string& path );

    /** basic check if path doesnt contain spaces or.
     * todo improve
     * @param[in] path
     * @return false if path is empty or contains " "
     */
    bool isDirectoryPathValid( const string& path );

    /** checks if file path is valid.
     * todo improve
     * @param[in] path
     * @return false if file cant be opened or path is empty or contains spaces else true
     */
    static bool isFilePathValid( const string& path );


};

