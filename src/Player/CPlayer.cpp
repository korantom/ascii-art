#include "CPlayer.h"
#include <fstream>
#include <algorithm>

using namespace std;

CPlayer::CPlayer( const string& path )
        : m_rows( 0 ),
          m_cols( 0 ),
          m_window_menu( nullptr ),
          m_state( INIT_MESSAGE ),
          m_nextState( INIT_MESSAGE ),
          m_playerOn( false ),
          m_path( path ),
          m_playbackSpeed( sizeof( m_playbackSpeedList ) / sizeof( m_playbackSpeedList[0] ) - 1 ) {
    DIR *d;
    struct dirent *dir;
    d = opendir( m_path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = m_path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) //todo dont add hiden files
                m_ASCCIFilePaths.emplace_back( p );

        }
        closedir( d );
        sort( m_ASCCIFilePaths.begin(), m_ASCCIFilePaths.end());
    }

}

void CPlayer::Run() {
    this->InitializeMenu();
    while ( m_playerOn ) {
        switch (m_state) {
            case INIT_MESSAGE:
                this->InfoScreen( m_window_menu, m_infoMessage, m_rows, m_cols );
                m_nextState = PLAY;
                break;

            case ERROR:
                this->InfoScreen( m_window_menu, "EROOR no errors should ocure\n", m_rows, m_cols );
                m_nextState = INIT_MESSAGE;
                break;

            case EXIT:
                m_playerOn = false;
                wclear( m_window_menu );
                endwin();
                break;

            case PLAY:
                wclear( m_window_menu );
                m_nextState = PAUSE;
                this->play();
                break;
            case PAUSE:
                m_nextState = PLAY;
                this->pause();
                break;
            default:
                break;
        }
        m_state = m_nextState;
    }
}

void CPlayer::InitializeMenu() {
    noecho();
    m_playerOn = true;
    initscr();
    start_color();
    use_default_colors();
    init_pair( PAIR_BW, COLOR_BLACK, COLOR_WHITE );
    refresh();

    //get sizes of window
    getmaxyx( stdscr, m_rows, m_cols );

    m_window_menu = newwin( m_rows, m_cols, 0, 0 );
    wbkgd( m_window_menu, COLOR_PAIR( PAIR_BW ));
    box( m_window_menu, 0, 0 );

    //allow arrow navigation
    keypad( m_window_menu, true );


#ifdef DEBUG
    for ( int i = 1; i < m_rows - 1; ++i )//debuging
        mvwprintw( m_window_menu, i, 1, to_string( i ).c_str());
#endif
    wrefresh( m_window_menu );
//todo  podminka na minimalni velikost terminaludomyslet
//        if ( m_cols < MIN_WIDTH || m_rows < MIN_HEIGHT ) {
//            m_state = ERROR;
//        }
}

void CPlayer::InfoScreen( WINDOW *window, const string& message, int& rows, int& cols ) {
    wclear( window );
    getmaxyx( stdscr, rows, cols );

    int i = rows / 10;
    string line;
    std::stringstream ss( message );

    while ( getline( ss, line )) {
        mvwprintw( window, i, cols / 3, "%s", line.c_str());
        i++;
    }

    int keyPressed = 0;
    bool condition = true;
    while ( condition ) {
        keyPressed = wgetch( window );
        if ( keyPressed == KEY_RIGHT || keyPressed == KEY_LEFT )
            condition = false;

        if ( keyPressed == 'q' ) {
            m_nextState = EXIT;
            condition = false;
        }
    }
}

void CPlayer::display( WINDOW *window, const string& ASCIIFilePath ) {
    //todo improve centering of image
    wclear( window );
    int row, col;
    getmaxyx( stdscr, row, col );
    string line;
    int i = 2;
    int padding = 0;
    int strlen = 0;
    row = 0;//warning patch


    std::ifstream fs( ASCIIFilePath, std::ios::in );
    std::getline( fs, line );
    strlen = line.length();
    padding = (col - strlen) / 2;
    padding = (padding > 0) ? padding : 0;
    fs.close();

    wclear( window );
    std::ifstream FileStream( ASCIIFilePath, std::ios::in );
    mvwprintw( window, row, padding, ASCIIFilePath.c_str());//todo debug

    while ( std::getline( FileStream, line )) {
        mvwprintw( window, i, padding, line.c_str());
        i++;
    }
    FileStream.close();
    wrefresh( window );
    refresh();

}

void CPlayer::play() {
    wclear( m_window_menu );

    int keyPressed = 0;
    bool condition = true;
    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = end - start;
    nodelay( m_window_menu, TRUE );

    while ( condition ) {

        for ( string& asciiFile: m_ASCCIFilePaths ) {

            this->display( m_window_menu, asciiFile );

            start = std::chrono::system_clock::now();
            while ( 1 ) {
                keyPressed = wgetch( m_window_menu );
                if ( keyPressed == 'p' ) {
                    condition = false;
                    break;
                }
                if ( keyPressed == 'q' ) {
                    m_nextState = EXIT;
                    condition = false;
                    break;
                }
                if ( keyPressed == 'f' ) {
                    m_playbackSpeed++;
                    m_playbackSpeed = m_playbackSpeed % m_numOfSpeeds;
                }
                end = std::chrono::system_clock::now();
                diff = end - start;
                if ( diff.count() > m_playbackSpeedList[m_playbackSpeed] )
                    break;
            }
            if ( condition == false )
                break;

        }
    }

}

void CPlayer::pause() {
//        wclear(m_window_menu);
//        wrefresh( m_window_menu );
//        refresh();
    int keyPressed = 0;
    bool condition = true;
    nodelay( m_window_menu, FALSE );
    sleep( 1 ); //musi byt nejaky delay abych se neproklikl
    while ( condition ) {
        keyPressed = wgetch( m_window_menu );
        if ( keyPressed == 's' )
            condition = false;
        if ( keyPressed == 'q' ) {
            m_nextState = EXIT;
            condition = false;
        }
        if ( keyPressed == 'd' ) {
            //todo delete
//                condition = false;
        }
    }
}

