#include "CMenu.h"

#include <fstream>

using namespace std;

CMenu::CMenu( vector <string> *inputImagePaths, vector <pair<string, bool>> *transformationMenu, bool *conversionSuccess, string *shaderPath )
        : m_rows( 0 ),
          m_cols( 0 ),
          m_window_menu( nullptr ),

          m_state( INIT_MESSAGE ),
          m_nextState( INIT_MESSAGE ),

          m_menuOn( false ),

          m_imageDirectoryPath( "" ),
          m_imagePaths(),
          m_shaderPath( *shaderPath ),

          m_inputImagePaths( *inputImagePaths ),
          m_transformationMenu( *transformationMenu ),
          m_highlight( 0 ),
          m_conversionSuccess( *conversionSuccess ) {
    m_conversionSuccess = false;
}

CMenu::~CMenu() = default;

void CMenu::Run() {
    string input = "";
    this->InitializeMenu();

    while ( m_menuOn ) {
        switch (m_state) {
            case INIT_MESSAGE:

                this->InfoScreen( m_window_menu, m_infoMessage, m_rows, m_cols );
                m_nextState = SELECTION;
                break;

            case DIRECTORY_PATH:
                input = "";
                this->InputMenu( m_window_menu, input, "Enter Folder Path: " ); //or select individual imgs
                if ( isDirectoryPathValid( input )) {
                    m_nextState = SELECTION;
                    m_imageDirectoryPath = input;
                } else
                    m_nextState = ERROR;
                break;
            case IMAGE_PATH:
                input = "";
                this->InputMenu( m_window_menu, input, "Enter Image Path: " ); //or select individual imgs
                if ( isFilePathValid( input )) {
                    m_nextState = SELECTION;
                    m_imagePaths.emplace_back( input );
                } else
                    m_nextState = ERROR;
                break;
            case SHADERS:
                input = "";
                this->InputMenu( m_window_menu, input, "Enter Shader file Path: " ); //or select individual imgs
                if ( isFilePathValid( input )) {
                    m_nextState = SELECTION;
                    m_shaderPath = input;
                } else
                    m_nextState = ERROR;
                break;

            case SELECTION:
                this->TileMenu( m_window_menu, m_nextState, EXIT, m_mainMenu, m_rows, m_cols );
                break;

            case TRANSFORMATIONS:
                this->CheckBoxMenu( m_window_menu, m_nextState, SELECTION, m_transformationMenu, m_rows, m_cols );
                break;

            case CONVERSION:
                if ((m_imageDirectoryPath.empty() || !isDirectoryPathValid( m_imageDirectoryPath )) && m_imagePaths.empty()) {
                    m_nextState = ERROR;
                    break;
                }

                //todo do metody
                if ( isDirectoryPathValid( m_imageDirectoryPath )) {
                    DIR *d;
                    struct dirent *dir;
                    d = opendir( m_imageDirectoryPath.c_str());
                    if ( d ) {
                        while ((dir = readdir( d )) != NULL ) {
                            string p = m_imageDirectoryPath + "/" + dir->d_name;
//                                if ( strncmp( dir->d_name, "..", 2 ) != 0 && strncmp( dir->d_name, ".", 2 ) != 0 ) //todo hidden files dont add
                            if ((dir->d_name)[0] != '.' )
                                m_imagePaths.emplace_back( p );

                        }
                        closedir( d );
                    }
                }

                for ( string& p : m_imagePaths ) {
                    m_inputImagePaths.emplace_back( p );
                }
                m_nextState = EXIT;
                m_conversionSuccess = !m_inputImagePaths.empty();
//                    this->InfoScreen( m_window_menu, str, m_rows, m_cols );
                break;

            case ERROR:
                this->InfoScreen( m_window_menu, m_errorMessage, m_rows, m_cols );
                //for now one error state in future can make individual err states
                m_nextState = SELECTION;
                break;

            case EXIT:
                m_menuOn = false;
                break;

            default:
                break;
        }

        m_state = m_nextState;
    }

    endwin();
}

void CMenu::InitializeMenu() {

    m_menuOn = true;
    initscr();
    start_color();
    use_default_colors();
    init_pair( PAIR_BW, COLOR_BLACK, COLOR_WHITE );
    refresh();

    //get sizes of window
    getmaxyx( stdscr, m_rows, m_cols );

    m_window_menu = newwin( m_rows, m_cols, 0, 0 );
    wbkgd( m_window_menu, COLOR_PAIR( PAIR_BW ));
    box( m_window_menu, 0, 0 );

    //allow arrow navigation
    keypad( m_window_menu, true );
    noecho();

#ifdef DEBUG
    for ( int i = 1; i < m_rows - 1; ++i )//debuging
        mvwprintw( m_window_menu, i, 1, to_string( i ).c_str());
#endif
    wrefresh( m_window_menu );
//todo  podminka na minimalni velikost terminaludomyslet
//        if ( m_cols < MIN_WIDTH || m_rows < MIN_HEIGHT ) {
//            m_state = ERROR;
//        }
}

void CMenu::InfoScreen( WINDOW *window, const string& message, int& rows, int& cols ) {
    wclear( window );
    getmaxyx( stdscr, rows, cols );


    int i = rows / 10;
    string line;
    std::stringstream ss( message );

    while ( getline( ss, line )) {
        mvwprintw( window, i, cols / 3, "%s", line.c_str());
        i++;
    }

    int keyPressed = 0;
    bool condition = true;
    while ( condition ) {
        keyPressed = wgetch( window );
        if ( keyPressed == KEY_RIGHT || keyPressed == KEY_LEFT )
            condition = false;
    }
}

void CMenu::TileMenu( WINDOW *window, EState& nextState, EState back, const vector <pair<string, EState>>& menu, int& rows, int& cols ) {
    wclear( window );
    getmaxyx( stdscr, rows, cols );

    int keyPressed = 0;
    bool condition = true;


    int menuLength = menu.size();

    while ( condition ) {
        for ( int i = 0; i < menuLength; ++i ) {
            if ( i == m_highlight )
                wattron( window, A_REVERSE );
            mvwprintw( window, i + rows / 10, cols / 3, menu[i].first.c_str());
            wattroff( window, A_REVERSE );
        }
        keyPressed = wgetch( window );
        switch (keyPressed) {
            case KEY_UP:
                m_highlight--;
                m_highlight =
                        (menuLength + m_highlight % menuLength) % menuLength;//            m_highlight = (m_highlight<0)?3:m_highlight;
                break;
            case KEY_DOWN:
                m_highlight++;
                m_highlight = m_highlight % menuLength;
                break;
            case KEY_LEFT: //backspace or escape
                condition = false;
                nextState = back;
                break;
            case KEY_RIGHT: //Enter
                condition = false;
                nextState = menu[m_highlight].second;
                break;
            default:
                break;
        }
    }


}

void CMenu::CheckBoxMenu( WINDOW *window, EState& nextState, EState back, vector <pair<string, bool>>& optinMenu, int& rows, int& cols ) {
    wclear( window );
    getmaxyx( stdscr, rows, cols );
    string x, o;
    x = "x";
    o = " ";
    int keyPressed = 0;
    int highlight = 0;
    bool condition = true;
    int menuLength = optinMenu.size();

    while ( condition ) {
        for ( int i = 0; i < menuLength; ++i ) {
            if ( i == highlight )
                wattron( window, A_REVERSE );
            mvwprintw( window, i + rows / 10, cols / 3 + 2, optinMenu[i].first.c_str());
            mvwprintw( window, i + rows / 10, cols / 3, (optinMenu[i].second) ? x.c_str() : o.c_str());
            wattroff( window, A_REVERSE );
        }
        keyPressed = wgetch( window );
        switch (keyPressed) {
            case KEY_UP:
                highlight--;
                highlight = (menuLength + highlight % menuLength) % menuLength;
                break;
            case KEY_DOWN:
                highlight++;
                highlight = highlight % menuLength;
                break;
            case KEY_LEFT: //backspace or escape
                condition = false;
                nextState = back;
                break;
            case KEY_RIGHT: //enter
                optinMenu[highlight].second = !(optinMenu[highlight].second);
                break;
            default:
                break;
        }
    }
}

void CMenu::InputMenu( WINDOW *window, string& input, const string& message ) {
    echo();
    wclear( window );
    //todo osetrit buffer overflow nacitat znak po znaku
    char str[200];
    int row, col;
    getmaxyx( stdscr, row, col );
    mvwprintw( window, row / 10, (col - strlen( message.c_str())) / 2, "%s", message.c_str());

    //todo pridat moznost vratit se zpet bez nutnosti vyplneni
    wgetstr( window, str );
    input = string( str );
    noecho();
}

bool CMenu::fileExists( const string& path ) {
    fstream fs( path.c_str());
    bool result = fs.good();
    fs.close();
    return result;
}

bool CMenu::isDirectoryPathValid( const string& path ) {
    //todo chekni jestli je jmeno validni a jestli existuje adresar
    if ( path == "" || path.find( " " ) < path.length()) {
        return false;
    } else {
//            DIR *d;
//            struct dirent *dir;
//            d = opendir( path.c_str());
//            if ( d )
//                return false;
//            else
//                closedir( d );
        return true;
    }
}

bool CMenu::isFilePathValid( const string& path ) {
    return path == "" || path.find( " " ) < path.length() || !fileExists( path ) ? false : true;
}

