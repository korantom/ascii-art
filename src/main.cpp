#include <iostream>

#include "Format/CBMPImage.h"
#include "CPixelMatrix.h"
#include "CShader.h"
#include "CASCII.h"
#include "Player/CPlayer.h"
#include "Player/CMenu.h"
#include "Format/CJPGImage.h"
#include "Transformations/transformationAll.h"

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cmath>
#include <cctype>
#include <climits>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>
#include <memory>

#include <ncurses.h>
#include <dirent.h>
#include <stdio.h>
#include <cstring>

//#define TEST ///< if def test files executed else normal app

//------------------------------------------------------------------------------------------------------------------------------------

/**
* @file main.cpp
* @author Tomas Kornada
*
* @mainpage ASCII Art
*
*/

#ifndef TEST

//todo in what part of programme check file esistence etc..
bool fileExists( const string& path ) {
    if ( path.empty())
        return false;
    fstream fs( path.c_str());
    bool result = fs.good();
    fs.close();
    return result;
}

bool LoadImages( vector <string>& InputImagePaths, vector<CImage *>& InputImages ) {

    std::sort( InputImagePaths.begin(), InputImagePaths.end());
    string extension;
    for ( auto& imagePath : InputImagePaths ) {
//        cout << imagePath << endl;
        if ( !fileExists( imagePath ))
            continue;

        extension = imagePath.substr( imagePath.length() - 3, 3 );

        if ( extension == "bmp" )
            InputImages.emplace_back( new CBMPImage( imagePath ));
        else if ( extension == "jpg" )
            InputImages.emplace_back( new CJPGImage( imagePath ));
//        else if(extension == "png")

    }
    return !InputImages.empty();
}


void init( vector<CTransformation *>& AllTransformations, vector <pair<string, bool>>& TransformationMenu ) {
    AllTransformations.push_back( new CLinearGrayLevelTransformation());
    AllTransformations.push_back( new CLogarithmicGrayLevelTransformation());
    AllTransformations.push_back( new CPowerGrayLevelTransformation());
    AllTransformations.push_back( new CBrightnessTransformation());
    AllTransformations.push_back( new CVerticalFlipTransformation());
    AllTransformations.push_back( new CVHorizontalFlipTransformation());

    for ( auto transformation_ptr : AllTransformations ) {
        TransformationMenu.emplace_back( transformation_ptr->GetName(), false );
    }
}

//for general use
void ConvertImages( vector<CImage *>& InputImages, vector<CPixelMatrix *>& IntermediateImages ) {
    for ( auto image_ptr : InputImages ) {
        CPixelMatrix *m = new CPixelMatrix;
        image_ptr->ToPixelArray( *m );
        IntermediateImages.emplace_back( m );
    }
}

void MakeTransformations( vector<CTransformation *> AllTransformations, vector <pair<string, bool>> TransformationMenu,
                          vector<CTransformation *>& TransformationToApply ) {//Get/Make/Create etc/

    for ( size_t i = 0; i < TransformationMenu.size(); ++i ) {
        if ( TransformationMenu[i].second == true ) {
//            TransformationToApply.emplace_back( AllTransformations[i] ); // acuatly will be
            TransformationToApply.emplace_back( AllTransformations[i]->clone());
            //clone will create same transformation but will use user params in sonstructor
        }
    }

}

void ApplyTransformations( vector<CPixelMatrix *>& IntermediateImages, const vector<CTransformation *>& TransformationToApply ) {

    for ( CPixelMatrix *m_ptr : IntermediateImages ) {
        CPixelMatrix& m = *m_ptr;
//        m.ApplyTransformations(TransformationToApply);
//        for (auto & t : TransformationToApply) {
//            t->Apply( m );
//        }
///////
        for ( size_t i = 0; i < TransformationToApply.size(); ++i ) {
            TransformationToApply[i]->Apply( m );
        }
    }


}

void ConvertToASCII( const vector<CPixelMatrix *>& IntermediateImages, string& destination_path, const string& shaderPath ) {


    DIR *d;
    struct dirent *dir;
    d = opendir( destination_path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = destination_path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) //todo dont add hiden files
            {
                remove( p.c_str());
            }

        }
        closedir( d );
    }

    CShader *shader_ptr = (fileExists( shaderPath )) ? new CShader( shaderPath ) : nullptr;

    for ( auto inter_format_ptr : IntermediateImages ) {
//        CASCII x( *inter_format_ptr, max_window_height/m_rows, max_window_width/m_cols ); //todo read how big was menu screen and send as param
        CASCII x( *inter_format_ptr );
        string path = destination_path + "/" + inter_format_ptr->m_name;
        if ( shader_ptr != nullptr )
            x.ToASCII( path, shader_ptr );
        else
            x.ToASCII( path );
    }
    delete shader_ptr;
}

void CleanUp( vector<CImage *>& InputImages, vector<CPixelMatrix *>& IntermediateImages, vector<CTransformation *>& AllTransformations,
              vector<CTransformation *>& TransformationToApply ) {
    for ( CImage *img_ptr : InputImages ) {
        delete img_ptr;
    }
    InputImages.clear();

    for ( CPixelMatrix *pmatrix_ptr : IntermediateImages ) {
        delete pmatrix_ptr;
    }
    IntermediateImages.clear();

    for ( CTransformation *t_ptr : AllTransformations ) {
        delete t_ptr;
    }
    AllTransformations.clear();

    for ( CTransformation *t_ptr : TransformationToApply ) {
        delete t_ptr;
    }
    TransformationToApply.clear();

}

int main( void ) {

    bool readPathsSuccess = false;
    vector <string> InputImagePaths;
    vector < CImage * > InputImages; //vector<CImage*> inputSequence;
    vector < CPixelMatrix * > IntermediateImages;

    vector < CTransformation * > AllTransformations; //todo make into constant
    vector < CTransformation * > TransformationToApply;

    vector <pair<string, bool>> TransformationMenu;

    init( AllTransformations, TransformationMenu );
    string ASCIIPath = "OUTPUT_IMAGES";
    string shaderPath = "";


    CMenu menu( &InputImagePaths, &TransformationMenu, &readPathsSuccess, &shaderPath );
    menu.Run();
    if ( !readPathsSuccess ) {
        CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
        cout << "Exit / or no images provided\n" << endl;
        return 0;
    }

    if ( !LoadImages( InputImagePaths, InputImages )) {
        cout << "no images found\n" << endl;
        CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
        return 0;
    }
    ConvertImages( InputImages, IntermediateImages ); //todo jpg imges fsanitize here
    MakeTransformations( AllTransformations, TransformationMenu, TransformationToApply );
    ApplyTransformations( IntermediateImages, TransformationToApply );
    ConvertToASCII( IntermediateImages, ASCIIPath, shaderPath );
//
    CPlayer player( ASCIIPath );
    player.Run();

//todo uvolneni
    CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
    return 0;
}

#endif

/* todo
 * waitscreen
 */

//-------------------------------------------------------------------------------------------------------------------------------------------
#ifdef TEST
#include "TEST/tests.h"
int main() {
    using namespace std;
    extraTest();
    CBMPFormatTests();//redone
    PixelMatrixTests();//redone

   TransformationTest();

    BMPConversionTest();//redone
    JPGConversionTest();//redone
    FullSimulationTest();
//----------------------------------------------------------------------------------
    std::cout << "Main end" << std::endl;
    return 0;
}
#endif

