#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <string>
#include <algorithm>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
class CVerticalFlipTransformation : public CTransformation {
public:
    CVerticalFlipTransformation() = default;

    /** Flips image vertically.
     * @param[out] m
     */
    void Apply( CPixelMatrix& m ) override {
        std::reverse( m.m_matrix.begin(), m.m_matrix.end());
    }

    string GetName() const override {
        return m_name;
    }

    ~CVerticalFlipTransformation() override = default;

    CVerticalFlipTransformation( const CVerticalFlipTransformation& other ) = default;

    CVerticalFlipTransformation *clone() override {
        return new CVerticalFlipTransformation( *this );
    }

private:

    const string m_name = "Vertical Flip Transformation";

};
