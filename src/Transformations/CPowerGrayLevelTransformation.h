#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <cmath>
#include <string>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
class CPowerGrayLevelTransformation : public CTransformation {
public:

    explicit CPowerGrayLevelTransformation( uint8_t multiplicator = 2, double exponent = 1.05 ) : m_multiplicator( multiplicator ),
                                                                                                  m_exponent( exponent ) {}

    /** Gamma Transformation.
     * pixel = multiplier * pixel^gamma
     * @param[out] m pixel matrix on which the transformation is applied
     */
    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& row: m.m_matrix ) {
            for ( auto& rowVal: row ) {
                rowVal = (uint8_t)( m_multiplicator * pow((double) rowVal, m_exponent ));

            }
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CPowerGrayLevelTransformation() override = default;

    CPowerGrayLevelTransformation( const CPowerGrayLevelTransformation& other ) = default;

    CPowerGrayLevelTransformation *clone() override {
        return new CPowerGrayLevelTransformation( *this );
    }

private:
    uint8_t m_multiplicator;
    double m_exponent;                                 ///< gamma
    const string m_name = "Gamma Transformation";

};
