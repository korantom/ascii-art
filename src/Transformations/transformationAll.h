#pragma once

#include "CTransformation.hpp"
#include "CLogarithmicGrayLevelTransformation.h"
#include "CLinearGrayLevelTransformation.h"
#include "CPowerGrayLevelTransformation.h"
#include "CBrightnessTransformation.h"
#include "CContrastTransformation.h"
#include "CVerticalFlipTransformation.h"
#include "CHorizontalFlipTransformation.h"


