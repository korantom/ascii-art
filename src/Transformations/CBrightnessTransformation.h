#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <cmath>
#include <string>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
class CBrightnessTransformation : public CTransformation {
public:
    explicit CBrightnessTransformation( int brightnessShift = 45 ) : m_brightnessShift( brightnessShift ) {}

    /** Brightness Transformation.
     * lightens image
     * @param[out] m pixel matrix on which the transformation is applied
     */
    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& row: m.m_matrix ) {
            for ( auto& rowVal: row ) {
                if ((int) rowVal + m_brightnessShift >= 255 )
                    rowVal = 255;
                else
                    rowVal += m_brightnessShift;
            }
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CBrightnessTransformation() override = default;

    CBrightnessTransformation( const CBrightnessTransformation& other ) = default;

    CBrightnessTransformation *clone() override {
        return new CBrightnessTransformation( *this );
    }

private:
    int m_brightnessShift;
    const string m_name = "Brightness Transformation";

};
