#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <string>
#include <algorithm>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
class CVHorizontalFlipTransformation : public CTransformation {
public:
    CVHorizontalFlipTransformation() = default;

    /** Horizontal Transformation.
     * @param[out] m pixel matrix on which the transformation is applied
     */
    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& row: m.m_matrix ) {
            std::reverse( row.begin(), row.end());
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CVHorizontalFlipTransformation() override = default;

    CVHorizontalFlipTransformation( const CVHorizontalFlipTransformation& other ) = default;

    CVHorizontalFlipTransformation *clone() override {
        return new CVHorizontalFlipTransformation( *this );
    }

private:

    const string m_name = "Horizontal Flip Transformation";

};
