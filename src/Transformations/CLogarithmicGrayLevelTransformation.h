#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <cmath>
#include <string>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
class CLogarithmicGrayLevelTransformation : public CTransformation {
public:
    explicit CLogarithmicGrayLevelTransformation( uint32_t multiplicator = 100 ) : m_multiplicator( multiplicator ) {}

    /** Logarithmic Transformation.
     * pixel = multiplicator * log( pixel + 1 )
     * @param[out] m pixel matrix on which the transformation is applied
     */
    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& row: m.m_matrix ) {
            for ( auto& rowVal: row ) {
                rowVal = (uint8_t) (m_multiplicator * log((double) (1 + rowVal)));

            }
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CLogarithmicGrayLevelTransformation() override = default;

    CLogarithmicGrayLevelTransformation( const CLogarithmicGrayLevelTransformation& other ) = default;

    CLogarithmicGrayLevelTransformation *clone() override {
        return new CLogarithmicGrayLevelTransformation( *this );
    }

private:
    uint32_t m_multiplicator;
    const string m_name = "Logarithmic Gray Level Transformation";

};
