#pragma once

#include <string>

class CPixelMatrix;

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------

/** Abstract Parent class for all transformations.
 * provides basic interface\n
 *      apply transformation on any CPixelMatrix instance\n
 *      Get the name of transformation\n
 *      and clone transformation\n
 */
class CTransformation {
public:
    /** Applies transformation on given pixel matrix.
     * @param[out] m pixel matrix on which the transformation is applied
     */
    virtual void Apply( CPixelMatrix& m ) = 0;

    /** simple getter.
     * @return transformation name
     */
    virtual string GetName() const = 0;

    virtual ~CTransformation() = default;

    /** Clones transformation.
     * @return new copied instance of transformation
     */
    virtual CTransformation *clone() = 0;

};
