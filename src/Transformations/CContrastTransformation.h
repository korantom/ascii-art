#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <cmath>
#include <string>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
//todo not finished
class CContrastTransformation : public CTransformation {
public:
    explicit CContrastTransformation( int contrastMin = 10, int contrastMax = 230 ) : m_contrastMin( contrastMin ),
                                                                                      m_contrastMax( contrastMax ) {}

    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& i: m.m_matrix ) {
            for ( auto& j: i ) {
                if ( j < m_contrastMin )
                    j = 0;
                else if ( j > m_contrastMax )
                    j = 255;
//                else
//                    j = j;
            }
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CContrastTransformation() override = default;

    CContrastTransformation( const CContrastTransformation& other ) = default;

    CContrastTransformation *clone() override {
        return new CContrastTransformation( *this );
    }

private:
    int m_contrastMin;
    int m_contrastMax;
    const string m_name = "Contrast Transformation";

};
