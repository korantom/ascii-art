#pragma once

#include "CTransformation.hpp"
#include "../CPixelMatrix.h"
#include <cmath>

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------------

class CLinearGrayLevelTransformation : public CTransformation {
public:
    explicit CLinearGrayLevelTransformation( uint8_t shift = 255 ) : m_shift( shift ) {}

    /** Linear Transformation.
     * pixel = |shift - pixel|
     * @param[out] m pixel matrix on which the transformation is applied
     */
    inline void Apply( CPixelMatrix& m ) override {
        for ( auto& row: m.m_matrix ) {
            for ( auto& rowVal: row ) {
                rowVal = (uint8_t) abs((int) (m_shift - rowVal));

            }
        }
    }

    string GetName() const override {
        return m_name;
    }

    ~CLinearGrayLevelTransformation() override = default;

    CLinearGrayLevelTransformation( const CLinearGrayLevelTransformation& other ) = default;

    CLinearGrayLevelTransformation *clone() override {
        return new CLinearGrayLevelTransformation( *this );
    }

private:
    uint8_t m_shift;
    const string m_name = "Linear Gray Level Transformation";
};
