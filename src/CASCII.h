#pragma once

#include "CPixelMatrix.h"
#include "CShader.h"

#include <cmath>

#define ASCII_HEIGHT    70  ///< maximum default height of ascii txt file
#define ASCII_WIDTH     150 ///< maximum default width of  ascii txt file

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------------

/** ASCII class coverts intermediate formats of images(CPixelMatrix) into ascii txt/html files.
 */
class CASCII {
public:

    /** Constructor which receives image represented by the CPixelMatrix class and constraints on the maximum heigth/width of the txt files.
     *
     * @param[in] matrix    simplified image data in an intermediate formar
     * @param[in] maxHeight maximum height of txt file
     * @param[in] maxWidth  maximum width of txt file
     */
    CASCII( const CPixelMatrix& matrix, size_t maxHeight, size_t maxWidth );

    /** Constructor which receives image represented by the CPixelMatrix class, default constarints are added.
     *
     * @param[in] matrix    simplified image data in an intermediate formar
     */
    explicit CASCII( const CPixelMatrix& matrix );
//    explicit CASCII( const CPixelMatrix& matrix ) : m_maxHeight(ASCII_HEIGHT), m_maxWidth(ASCII_WIDTH), m_dataOriginal( matrix ), m_shadder( nullptr ), m_defaultShadder( new CShadder()) {};

    ~CASCII();

//------------------------------------------------------------------------------------------------------------------------------------------
    //todo ... chose wchich conversion to use
    /** converts loaded image to an ascii file.
     * @param filePath destination path where to save the ascii file
     * @param shader   instance of shader to use, if none is provided deafault one will be used
     */
    void ToASCII( const string& filePath, CShader *shader = nullptr );

    /** converts and compresses loaded image to an html file.
     * @param filePath destination path where to save the ascii file
     * @param shader   instance of shader to use, if none is provided deafault one will be used
     */
    void ToASCIIHTML( const string& filePath, CShader *shader = nullptr ); //html

    /** converts and compresses loaded image to an txt file,.
     * @param filePath destination path where to save the ascii file
     * @param shader   instance of shader to use, if none is provided deafault one will be used
     */
    void ToASCIITXT( const string& filePath, CShader *shader = nullptr );//normal

    /** converts loaded image to a txt file without compressing the image.
     * @param filePath destination path where to save the ascii file
     * @param shader   instance of shader to use, if none is provided deafault one will be used
     */
    void ToASCIIFULLRESOLUTION( const string& filePath, CShader *shader = nullptr );//no compression
    

private:
    int m_maxHeight;                    ///< maximum height of ascii file
    int m_maxWidth;                     ///< width height of ascii file
    CPixelMatrix m_dataOriginal;
//    CPixelMatrix m_dataCompressed;
    CShader *m_shadder;                 ///< user provided shader
    CShader *const m_defaultShadder;    ///< default shader


};
