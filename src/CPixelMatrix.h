#pragma once

#include "Transformations/CTransformation.hpp"

#include <vector>
#include <string>
#include <cmath>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------

/** Pixel Matrix stores raw image data.
 * doesnt hold any extra info except width height and file name
 * allows user to compress or transfrom image data
 */
class CPixelMatrix {
public:

    /** No param constructor.
     * initializes atributes to zero/mepty values
     */
    CPixelMatrix();

    /** Copy constructor.
     * @param[in] other
     */
    CPixelMatrix( const CPixelMatrix& other );

    /** = operator.
     * @param other
     * @return
     */
    CPixelMatrix& operator=( const CPixelMatrix& other );


//------------------------------------------------------------------------------------------------------------------------------------------

    /** Constructor that compresses image data.
     * @param[in] other     CPixelMatrix that is copied and compressed
     * @param[in] maxHeight maximum height of the compressed image
     * @param[in] maxWidth maximum width of the compressed image
     */
    CPixelMatrix( const CPixelMatrix& other, const int& maxHeight, const int& maxWidth );

    /** Applies a series of transformations.
     *
     * @param[in] TransformationToApply vector of transformation pointers
     * each transformation is an objcet which modifies the pixel matrix data
     */
    void ApplyTransformations( const vector<CTransformation *>& TransformationToApply );

    /** deletes data.
     */
    void Clear();

    //CBMPImage* toBMP(  ); //todo ?
//------------------------------------------------------------------------------------------------------------------------------------------
    int m_height;                        ///< image height not in uint8_ts
    int m_width;                         ///< image width not in uint8_ts
    string m_name;                       ///< file name
    vector <vector<uint8_t>> m_matrix;  ///< iamge data
};


