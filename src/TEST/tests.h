#pragma once

#include "../Format/CBMPImage.h"
#include "../Format/CJPGImage.h"
#include "../CPixelMatrix.h"
#include "../CShader.h"
#include "../CASCII.h"
#include "../Transformations/CTransformation.hpp"
#include "../Transformations/transformationAll.h"

#include <cstring>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <cassert>
#include <dirent.h>


/** Tests.
 * Tests various stages of program \n
 * reading images \n
 * representing them in an intermediate format \n
 * transformations \n
 * simulation of User input \n
 * @file tests.h
 *
 */

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------------
/** Deletes contents of given Directory.
 * @param[in] path
 */
void deleteDir( const string& path ) {
    DIR *d;
    struct dirent *dir;
    d = opendir( path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) {
                remove( p.c_str());
            }

        }
        closedir( d );
    }

}

//todo done
/** Tests CBMPImage class by loading various images and copying them.
 * tests 8, 24, 32 bit images and images with various widths
 */
void CBMPFormatTests() {
    deleteDir( "src/TEST/BMP_format_test" );
    CBMPImage X, Y, Z, W;
//basic tests
    X.LoadImage( "src/TEST/Images_BMP/grid_Half.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/grid_Half_copy.bmp" );
    Y.LoadImage( "src/TEST/BMP_format_test/grid_Half_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Half.bmp", "src/TEST/BMP_format_test/grid_Half_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/grid_Big.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/grid_Big_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Big.bmp", "src/TEST/BMP_format_test/grid_Big_copy.bmp" ));


//    X.LoadImage( "src/TEST/Images_BMP/grid_Fine.bmp" );
//    X.CopyImage( "src/TEST/BMP_format_test/grid_Fine_copy.bmp" );
//    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Fine.bmp", "src/TEST/BMP_format_test/grid_Fine_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/grid_Half2.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/grid_Half2_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Half2.bmp", "src/TEST/BMP_format_test/grid_Half2_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/grid_Half3.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/grid_Half3_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Half3.bmp", "src/TEST/BMP_format_test/grid_Half3_copy.bmp" ));

////------------------------------------------------------------------------------------------------------------------------------------
//test limit width "filler zeroes"
    X.LoadImage( "src/TEST/Images_BMP/grid_Squares1.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/grid_Squares1_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/grid_Squares1.bmp", "src/TEST/BMP_format_test/grid_Squares1_copy.bmp" ));


    X.LoadImage( "src/TEST/Images_BMP/width49.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/width49_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/width49.bmp", "src/TEST/BMP_format_test/width49_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/width50.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/width50_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/width50.bmp", "src/TEST/BMP_format_test/width50_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/width51.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/width51_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/width51.bmp", "src/TEST/BMP_format_test/width51_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/width52.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/width52_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/width52.bmp", "src/TEST/BMP_format_test/width52_copy.bmp" ));
////------------------------------------------------------------------------------------------------------------------------------------
//test 32 bit images
    X.LoadImage( "src/TEST/Images_BMP/Shapes.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/Shapes_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/Shapes.bmp", "src/TEST/BMP_format_test/Shapes_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/chess_board.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/chess_board_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/chess_board.bmp", "src/TEST/BMP_format_test/chess_board_copy.bmp" ));

    X.LoadImage( "src/TEST/Images_BMP/wave.bmp" );
    X.CopyImage( "src/TEST/BMP_format_test/wave_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/wave.bmp", "src/TEST/BMP_format_test/wave_copy.bmp" ));

////------------------------------------------------------------------------------------------------------------------------------------

    Z.LoadImage( "src/TEST/Images_BMP/nova-budova.bmp" );
    Z.CopyImage( "src/TEST/BMP_format_test/nova-budova_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/nova-budova.bmp", "src/TEST/BMP_format_test/nova-budova_copy.bmp" ));


    W.LoadImage( "src/TEST/Images_BMP/lena_gray.bmp" );
    W.CopyImage( "src/TEST/BMP_format_test/lena_gray_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/lena_gray.bmp", "src/TEST/BMP_format_test/lena_gray_copy.bmp" ));

    W.LoadImage( "src/TEST/Images_BMP/barbara_gray.bmp" );
    W.CopyImage( "src/TEST/BMP_format_test/barbara_gray_copy.bmp" );
    assert ( CBMPImage::identicalFiles( "src/TEST/Images_BMP/barbara_gray.bmp", "src/TEST/BMP_format_test/barbara_gray_copy.bmp" ));

    std::cout << "BMPFormat Test success" << std::endl;
}

//todo done
/** Tests CPixelMatrix class by loading various images bmp images and converting them into 8bit bmp images.
 * visual test
 */
void PixelMatrixTests() {
    CBMPImage::BMPFileHeader fileHeader;
    fileHeader.m_signature = 0x4D42;
    fileHeader.m_file_size = 0;
    fileHeader.m_reserved1 = 0;
    fileHeader.m_reserved2 = 0;
    fileHeader.m_offset_data = 0;
    CBMPImage::BMPInfoHeader infoHeader;
    infoHeader.m_info_header_size = 0;
    infoHeader.m_width = 0;
    infoHeader.m_height = 0;

    infoHeader.m_planes = 1;
    infoHeader.m_bits_per_pixel = 0;
    infoHeader.m_compression = 0;
    infoHeader.m_image_size = 0;
    infoHeader.m_x_pixels_per_meter = 0;
    infoHeader.m_y_pixels_per_meter = 0;
    infoHeader.m_colors_used = 0;
    infoHeader.m_colors_important = 0;

    CBMPImage::BMPColorTable colorTable;
    uint8_t dummy = 0;
    for ( int k = 0; k < 256; ++k ) {
        colorTable.colors[k][0] = dummy;
        colorTable.colors[k][1] = dummy;
        colorTable.colors[k][2] = dummy;
        colorTable.colors[k][3] = 0;
        dummy += 1;
    }
    deleteDir( "src/TEST/Pixel_matrix_test" );
//------------------------------------------------------------------------------------------------------------------------------------------
    CBMPImage Z( "src/TEST/Images_BMP/nova-budova32.bmp" );
    //working code form conversion from 24 bit to 8 bit
    CPixelMatrix m;
    Z.ToPixelArray( m );
    std::ofstream outputFileStream( "src/TEST/Pixel_matrix_test/nova-budova32_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z.GetWidth();
    infoHeader.m_height = Z.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream );

    colorTable.Write( outputFileStream );

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream.write((const char *) &m.m_matrix[i][j], sizeof( m.m_matrix[i][j] ));
        }
    }

    outputFileStream.close();

//------------------------------------------------------------------------------------------------------------------------------------------
    CBMPImage Z2( "src/TEST/Images_BMP/Shapes.bmp" );
    //working code form conversion from 32 bit to 8 bit
    CPixelMatrix m2;
    Z2.ToPixelArray( m2 );
    std::ofstream outputFileStream2( "src/TEST/Pixel_matrix_test/Shapes_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z2.GetWidth();
    infoHeader.m_height = Z2.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream2 );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream2 );

    colorTable.Write( outputFileStream2 );

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream2.write((const char *) &m2.m_matrix[i][j], sizeof( m2.m_matrix[i][j] ));
        }
    }

    outputFileStream2.close();
//------------------------------------------------------------------------------------------------------------------------------------------
    CBMPImage Z3( "src/TEST/Images_BMP/barbara_gray.bmp" );
    //working code form conversion from 32 bit to 8 bit
    CPixelMatrix m3;
    Z3.ToPixelArray( m3 );
    std::ofstream outputFileStream3( "src/TEST/Pixel_matrix_test/barbara_gray_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z3.GetWidth();
    infoHeader.m_height = Z3.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream3 );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream3 );

    colorTable.Write( outputFileStream3 );

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream3.write((const char *) &m3.m_matrix[i][j], sizeof( m3.m_matrix[i][j] ));
        }
    }
    outputFileStream3.close();
//------------------------------------------------------------------------------------------------------------------------------------------
    CBMPImage Z4( "src/TEST/Images_BMP/chess_board.bmp" );
    CPixelMatrix m4;
    Z4.ToPixelArray( m4 );
    std::ofstream outputFileStream4( "src/TEST/Pixel_matrix_test/chess_board_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z4.GetWidth();
    infoHeader.m_height = Z4.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream4 );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream4 );

    colorTable.Write( outputFileStream4 );

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream4.write((const char *) &m4.m_matrix[i][j], sizeof( m4.m_matrix[i][j] ));
        }
    }
    outputFileStream4.close();

//------------------------------------------------------------------------------------------------------------------------------------------
//!special case when not %4 width!
    CBMPImage Z5( "src/TEST/Images_BMP/gandalf.bmp" );
    //working code form conversion from 32 bit to 8 bit
    CPixelMatrix m5;
    Z5.ToPixelArray( m5 );
    std::ofstream outputFileStream5( "src/TEST/Pixel_matrix_test/gandalf_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z5.GetWidth();
    infoHeader.m_height = Z5.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream5 );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream5 );

    colorTable.Write( outputFileStream5 );

    uint8_t channel = 0;
    int pixelSize = infoHeader.m_bits_per_pixel / 8;
    int padding = 4 - (infoHeader.m_width * (pixelSize)) % 4;
    padding = (padding == 4) ? 0 : padding;

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream5.write((const char *) &m5.m_matrix[i][j], sizeof( m5.m_matrix[i][j] ));
        }
        //pading zeroes
        for ( int k = 0; k < padding; ++k ) {
            outputFileStream5.write((const char *) &channel, sizeof( channel ));
        }
    }

    outputFileStream5.close();


    //------------------------------------------------------------------------------------------------------------------------------------------
//!special case when not %4 width!
    CBMPImage Z6( "src/TEST/Images_BMP/width49.bmp" );
    //working code form conversion from 32 bit to 8 bit
    CPixelMatrix m6;
    Z6.ToPixelArray( m6 );
    std::ofstream outputFileStream6( "src/TEST/Pixel_matrix_test/width49_8bit.bmp", std::ios::out | std::ios::binary );

    infoHeader.m_width = Z6.GetWidth();
    infoHeader.m_height = Z6.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStream6 );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStream6 );

    colorTable.Write( outputFileStream6 );

    channel = 0;
    pixelSize = infoHeader.m_bits_per_pixel / 8;
    padding = 4 - (infoHeader.m_width * (pixelSize)) % 4;
    padding = (padding == 4) ? 0 : padding;

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStream6.write((const char *) &m6.m_matrix[i][j], sizeof( m6.m_matrix[i][j] ));
        }
        //pading zeroes
        for ( int k = 0; k < padding; ++k ) {
            outputFileStream6.write((const char *) &channel, sizeof( channel ));
        }
    }

    outputFileStream6.close();

    std::cout << "PixelMatrix Test success" << std::endl;

}

//todo
void ShaderTest();

//todo
void ASCIITest() {

    CBMPImage X, Y, Z, W;
    CPixelMatrix M;


//    CShader S0;
    X.LoadImage( "Images/grid_Half.bmp" );
    X.ToPixelArray( M );
    CASCII A1( M );
    A1.ToASCII( "Images/grid_Half" );


    X.LoadImage( "Images/grid_Big.bmp" );
    X.ToPixelArray( M );
    CASCII A2( M );
    A2.ToASCII( "Images/grid_Big" );


    X.LoadImage( "Images/grid_Fine.bmp" );


    X.LoadImage( "Images/linear_gradient.bmp" );
    X.ToPixelArray( M );
    CASCII A21( M );
    A21.ToASCII( "Images/linear_gradient" );


    X.LoadImage( "Images/grid_Half2.bmp" );


    X.LoadImage( "Images/grid_Half3.bmp" );




//------------------------------------------------------------------------------------------------------------------------------------
    X.LoadImage( "Images/grid_Squares1.bmp" );


    X.LoadImage( "Images/width49.bmp" );


    X.LoadImage( "Images/width50.bmp" );


    X.LoadImage( "Images/width51.bmp" );


    X.LoadImage( "Images/width52.bmp" );


    X.LoadImage( "Images/Shapes.bmp" );
    X.ToPixelArray( M );
    CASCII A3( M );
    A3.ToASCII( "Images/Shapes" );


    X.LoadImage( "Images/chess_board.bmp" );
    X.ToPixelArray( M );
    CASCII A4( M );
    A4.ToASCII( "Images/chess_board" );


    X.LoadImage( "Images/test_pnet.bmp" );


    X.LoadImage( "Images/wave.bmp" );
    X.ToPixelArray( M );
    CASCII A20( M );
    A20.ToASCII( "Images/wave" );


    Y.LoadImage( "Images/novabudova.bmp" );


//------------------------------------------------------------------------------
    Z.LoadImage( "Images/nova-budova.bmp" );
    Z.ToPixelArray( M );
    CASCII A5( M );
    A5.ToASCII( "Images/nova-budova" );

    Z.LoadImage( "Images/nova-budova32.bmp" );
    Z.ToPixelArray( M );
    CASCII A6( M );
    A6.ToASCII( "Images/nova-budova32" );


//------------------------------------------------------------------------------

    W.LoadImage( "Images/lena_gray.bmp" );
    W.ToPixelArray( M );
    CASCII A7( M );
    A7.ToASCII( "Images/lena_gray" );


    W.LoadImage( "Images/barbara_gray.bmp" );
    W.ToPixelArray( M );
    CASCII A8( M );
    A8.ToASCII( "Images/barbara_gray" );


    W.LoadImage( "Images/black.bmp" );
    W.ToPixelArray( M );
    CASCII A10( M );
    A10.ToASCII( "Images/black" );


    W.LoadImage( "Images/white.bmp" );
    W.ToPixelArray( M );
    CASCII A11( M );
    A11.ToASCII( "Images/white" );


    std::cout << "ASCII Test success" << std::endl;

}

//todo doen
/** Tests CTransformation classes by loading various images bmp images transforming them and converting them into 8bit bmp images.
 * visual test
 */
void TransformationTest() {
    CBMPImage::BMPFileHeader fileHeader;
    fileHeader.m_signature = 0x4D42;
    fileHeader.m_file_size = 0;
    fileHeader.m_reserved1 = 0;
    fileHeader.m_reserved2 = 0;
    fileHeader.m_offset_data = 0;
    CBMPImage::BMPInfoHeader infoHeader;
    infoHeader.m_info_header_size = 0;
    infoHeader.m_width = 0;
    infoHeader.m_height = 0;

    infoHeader.m_planes = 1;
    infoHeader.m_bits_per_pixel = 0;
    infoHeader.m_compression = 0;
    infoHeader.m_image_size = 0;
    infoHeader.m_x_pixels_per_meter = 0;
    infoHeader.m_y_pixels_per_meter = 0;
    infoHeader.m_colors_used = 0;
    infoHeader.m_colors_important = 0;

    CBMPImage::BMPColorTable colorTable;
    uint8_t dummy = 0;
    for ( int k = 0; k < 256; ++k ) {
        colorTable.colors[k][0] = dummy;
        colorTable.colors[k][1] = dummy;
        colorTable.colors[k][2] = dummy;
        colorTable.colors[k][3] = 0;
        dummy += 1;
    }
    deleteDir( "src/TEST/Transformation_test" );
//------------------------------------------------------------------------------------------------------------------------------------------
    CBMPImage Z( "src/TEST/Images_BMP/nova-budova32.bmp" );
    //working code form conversion from 24 bit to 8 bit
    CPixelMatrix mA, mB, mC, mD, mE;
    Z.ToPixelArray( mA );
    Z.ToPixelArray( mB );
    Z.ToPixelArray( mC );
    Z.ToPixelArray( mD );
    Z.ToPixelArray( mE );
    std::ofstream outputFileStreamA( "src/TEST/Transformation_test/nova-budova_linear_8bit.bmp", std::ios::out | std::ios::binary );
    std::ofstream outputFileStreamB( "src/TEST/Transformation_test/nova-budova_log_8bit.bmp", std::ios::out | std::ios::binary );
    std::ofstream outputFileStreamC( "src/TEST/Transformation_test/nova-budova_pow_8bit.bmp", std::ios::out | std::ios::binary );
    std::ofstream outputFileStreamD( "src/TEST/Transformation_test/nova-budova_brightness_8bit.bmp", std::ios::out | std::ios::binary );
    std::ofstream outputFileStreamE( "src/TEST/Transformation_test/nova-budova_flip_8bit.bmp", std::ios::out | std::ios::binary );
//----
    CLinearGrayLevelTransformation linT;
    CLogarithmicGrayLevelTransformation logT;
    CPowerGrayLevelTransformation powT;
    CBrightnessTransformation briT;
    CVerticalFlipTransformation verT;
    linT.Apply( mA );
    logT.Apply( mB );
    powT.Apply( mC );
    briT.Apply( mD );
    verT.Apply( mE );
//----




    infoHeader.m_width = Z.GetWidth();
    infoHeader.m_height = Z.GetHeight();


    fileHeader.m_offset_data = 1078;
    fileHeader.m_file_size = 1078 + 1 * infoHeader.m_width * infoHeader.m_height;
    fileHeader.Write( outputFileStreamA );
    fileHeader.Write( outputFileStreamB );
    fileHeader.Write( outputFileStreamC );
    fileHeader.Write( outputFileStreamD );
    fileHeader.Write( outputFileStreamE );

    infoHeader.m_info_header_size = 40;
    infoHeader.m_bits_per_pixel = 8;
    infoHeader.m_image_size = 0;
    infoHeader.Write( outputFileStreamA );
    infoHeader.Write( outputFileStreamB );
    infoHeader.Write( outputFileStreamC );
    infoHeader.Write( outputFileStreamD );
    infoHeader.Write( outputFileStreamE );

    colorTable.Write( outputFileStreamA );
    colorTable.Write( outputFileStreamB );
    colorTable.Write( outputFileStreamC );
    colorTable.Write( outputFileStreamD );
    colorTable.Write( outputFileStreamE );

    for ( int i = infoHeader.m_height - 1; i >= 0; --i ) {
        for ( int j = 0; j < infoHeader.m_width; ++j ) {
            outputFileStreamA.write((const char *) &mA.m_matrix[i][j], sizeof( mA.m_matrix[i][j] ));
            outputFileStreamB.write((const char *) &mB.m_matrix[i][j], sizeof( mB.m_matrix[i][j] ));
            outputFileStreamC.write((const char *) &mC.m_matrix[i][j], sizeof( mC.m_matrix[i][j] ));
            outputFileStreamD.write((const char *) &mD.m_matrix[i][j], sizeof( mD.m_matrix[i][j] ));
            outputFileStreamE.write((const char *) &mE.m_matrix[i][j], sizeof( mE.m_matrix[i][j] ));
        }
    }

    outputFileStreamA.close();
    outputFileStreamB.close();
    outputFileStreamC.close();
    outputFileStreamD.close();
    outputFileStreamE.close();
//------------------------------------------------------------------------------------------------------------------------------------------
    std::cout << "Transofrmation Test success" << std::endl;

}

//https://www.onlineconverter.com/ fungujici convertor z png a jpeg do bmp
//copied functions from main.cpp with some extra printing
bool fileExists( const string& path ) {
    if ( path.empty())
        return false;
    fstream fs( path.c_str());
    bool result = fs.good();
    fs.close();
    return result;
}

bool LoadImages( vector<string>& InputImagePaths, vector<CImage *>& InputImages ) {

    std::sort( InputImagePaths.begin(), InputImagePaths.end());
    string extension;
    for ( auto& imagePath : InputImagePaths ) {
//        cout << imagePath << endl;
        if ( !fileExists( imagePath ))
            continue;

        extension = imagePath.substr( imagePath.length() - 3, 3 );

        if ( extension == "bmp" )
            InputImages.emplace_back( new CBMPImage( imagePath ));
        else if ( extension == "jpg" )
            InputImages.emplace_back( new CJPGImage( imagePath ));
//        else if(extension == "png")

    }
    return !InputImages.empty();
}

void init( vector<CTransformation *>& AllTransformations, vector<pair<string, bool>>& TransformationMenu ) {
    AllTransformations.push_back( new CLinearGrayLevelTransformation());
    AllTransformations.push_back( new CLogarithmicGrayLevelTransformation());
    AllTransformations.push_back( new CPowerGrayLevelTransformation());
    AllTransformations.push_back( new CBrightnessTransformation());
    AllTransformations.push_back( new CContrastTransformation());

    for ( auto transformation_ptr : AllTransformations ) {
        TransformationMenu.emplace_back( transformation_ptr->GetName(), false );
    }
}

//for general use
void ConvertImages( vector<CImage *>& InputImages, vector<CPixelMatrix *>& IntermediateImages ) {
    for ( auto image_ptr : InputImages ) {
        CPixelMatrix *m = new CPixelMatrix;
        image_ptr->ToPixelArray( *m );
        IntermediateImages.emplace_back( m );
    }
}

void MakeTransformations( vector<CTransformation *> AllTransformations, vector<pair<string, bool>> TransformationMenu,
                          vector<CTransformation *>& TransformationToApply ) {//Get/Make/Create etc/

    for ( size_t i = 0; i < TransformationMenu.size(); ++i ) {
        if ( TransformationMenu[i].second == true ) {
//            TransformationToApply.emplace_back( AllTransformations[i] ); // acuatly will be
            TransformationToApply.emplace_back( AllTransformations[i]->clone());
            //clone will create same transformation but will use user params in sonstructor
        }
    }

}

void ApplyTransformations( vector<CPixelMatrix *>& IntermediateImages, const vector<CTransformation *>& TransformationToApply ) {

    for ( CPixelMatrix *m_ptr : IntermediateImages ) {
        CPixelMatrix& m = *m_ptr;
//        m.ApplyTransformations(TransformationToApply);
//        for (auto & t : TransformationToApply) {
//            t->Apply( m );
//        }
///////
        for ( size_t i = 0; i < TransformationToApply.size(); ++i ) {
            TransformationToApply[i]->Apply( m );
        }
    }


}

void ConvertToASCII( const vector<CPixelMatrix *>& IntermediateImages, string& destination_path, const string& shaderPath ) {


    DIR *d;
    struct dirent *dir;
    d = opendir( destination_path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = destination_path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) //todo dont add hiden files
            {
                remove( p.c_str());
            }

        }
        closedir( d );
    }

    CShader *shader_ptr = (fileExists( shaderPath )) ? new CShader( shaderPath ) : nullptr;

    for ( auto inter_format_ptr : IntermediateImages ) {
//        CASCII x( *inter_format_ptr, max_window_height/m_rows, max_window_width/m_cols ); //todo read how big was menu screen and send as param
        CASCII x( *inter_format_ptr );
        string path = destination_path + "/" + inter_format_ptr->m_name;
        if ( shader_ptr != nullptr )
            x.ToASCII( path, shader_ptr );
        else
            x.ToASCII( path );
    }
}

void CleanUp( vector<CImage *>& InputImages, vector<CPixelMatrix *>& IntermediateImages, vector<CTransformation *>& AllTransformations,
              vector<CTransformation *>& TransformationToApply ) {
    for ( CImage *img_ptr : InputImages ) {
        delete img_ptr;
    }
    InputImages.clear();

    for ( CPixelMatrix *pmatrix_ptr : IntermediateImages ) {
        delete pmatrix_ptr;
    }
    IntermediateImages.clear();

    for ( CTransformation *t_ptr : AllTransformations ) {
        delete t_ptr;
    }
    AllTransformations.clear();

    for ( CTransformation *t_ptr : TransformationToApply ) {
        delete t_ptr;
    }
    TransformationToApply.clear();

}


//todo done
/** Tests conversion from bmp to ascii files without applying any transformations.
 * visual test
 */
void BMPConversionTest() {

    vector<string> InputImagePaths;
    vector<CImage *> InputImages; //vector<CImage*> inputSequence;
    vector<CPixelMatrix *> IntermediateImages;

    vector<CTransformation *> AllTransformations; //todo DAVID tip: udelat jako konstantu
    vector<CTransformation *> TransformationToApply;

    vector<pair<string, bool>> TransformationMenu;

    init( AllTransformations, TransformationMenu );
    string ASCIIPath = "src/TEST/BMP_conversion_test";
    string shaderPath = "";
//---------------------------------------------------

//    CMenu menu( &InputImagePaths, &TransformationMenu, &readPathsSuccess, &shaderPath );
//    menu.Run();

    deleteDir( ASCIIPath );
    string path = "src/TEST/Images_BMP";
    DIR *d;
    struct dirent *dir;
    d = opendir( path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) {
                cout << "\t" << p << endl;
                InputImagePaths.emplace_back( p );
            }

        }
        closedir( d );
    }

//    zpracovani dat se deje zde
    if ( !LoadImages( InputImagePaths, InputImages )) {
        CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
        return;
    }
    ConvertImages( InputImages, IntermediateImages ); //todo jpg imges fsanitize here
    MakeTransformations( AllTransformations, TransformationMenu, TransformationToApply );
    ApplyTransformations( IntermediateImages, TransformationToApply );
    ConvertToASCII( IntermediateImages, ASCIIPath, shaderPath ); //todo bmp imges fsanitize here


    CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
    std::cout << "BMPConversionTest Test success" << std::endl;
}

//todo done
/** Tests conversion from jpg to ascii files without applying any transformations.
 * visual test
 */
void JPGConversionTest() {

    vector<string> InputImagePaths;
    vector<CImage *> InputImages; //vector<CImage*> inputSequence;
    vector<CPixelMatrix *> IntermediateImages;

    vector<CTransformation *> AllTransformations; //todo DAVID tip: udelat jako konstantu
    vector<CTransformation *> TransformationToApply;

    vector<pair<string, bool>> TransformationMenu;

    init( AllTransformations, TransformationMenu );
    string ASCIIPath = "src/TEST/JPG_conversion_test";
    string shaderPath = "";
//---------------------------------------------------

//    CMenu menu( &InputImagePaths, &TransformationMenu, &readPathsSuccess, &shaderPath );
//    menu.Run();

    deleteDir( ASCIIPath );
    string path = "src/TEST/Images_JPG";
    DIR *d;
    struct dirent *dir;
    d = opendir( path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) {
                cout << "\t" << p << endl;
                InputImagePaths.emplace_back( p );
            }

        }
        closedir( d );
    }

//    zpracovani dat se deje zde
    if ( !LoadImages( InputImagePaths, InputImages )) {
        CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
        return;
    }
    ConvertImages( InputImages, IntermediateImages ); //todo jpg imges fsanitize here :CONTAINER OVERFLOR
    MakeTransformations( AllTransformations, TransformationMenu, TransformationToApply );
    ApplyTransformations( IntermediateImages, TransformationToApply );
    ConvertToASCII( IntermediateImages, ASCIIPath, shaderPath );

    CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );

    std::cout << "JPGConversionTest Test success" << std::endl;
}

/** Simulates user, convers both bmp and jpg images and applies various transformations.
 * visual test
 */
void FullSimulationTest() {

    vector<string> InputImagePaths;
    vector<CImage *> InputImages; //vector<CImage*> inputSequence;
    vector<CPixelMatrix *> IntermediateImages;

    vector<CTransformation *> AllTransformations; //todo DAVID tip: udelat jako konstantu
    vector<CTransformation *> TransformationToApply;

    vector<pair<string, bool>> TransformationMenu;

    init( AllTransformations, TransformationMenu );
    string ASCIIPath = "src/TEST/Full_simulation_test";
    string shaderPath = "";
//---------------------------------------------------

//    CMenu menu( &InputImagePaths, &TransformationMenu, &readPathsSuccess, &shaderPath );
//    menu.Run();

    deleteDir( ASCIIPath );
    string path = "src/TEST/Images_JPG";
    DIR *d;
    struct dirent *dir;
    d = opendir( path.c_str());
    if ( d ) {
        while ((dir = readdir( d )) != NULL ) {
            string p = path + "/" + dir->d_name;
            if ((dir->d_name)[0] != '.' ) {
                cout << "\t" << p << endl;
                InputImagePaths.emplace_back( p );
            }

        }
        closedir( d );
    }

    InputImagePaths.emplace_back( "src/TEST/Images_BMP/a.bmp" );
    InputImagePaths.emplace_back( "src/TEST/Images_BMP/nova-budova.bmp" );
    InputImagePaths.emplace_back( "src/TEST/Images_BMP/lena_gray.bmp" );

//    zpracovani dat se deje zde
    if ( !LoadImages( InputImagePaths, InputImages )) {
        CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );
        return;
    }

    TransformationMenu[0].second = true;

    ConvertImages( InputImages, IntermediateImages ); //todo jpg imges fsanitize here :CONTAINER OVERFLOR
    MakeTransformations( AllTransformations, TransformationMenu, TransformationToApply );
    ApplyTransformations( IntermediateImages, TransformationToApply );
    ConvertToASCII( IntermediateImages, ASCIIPath, shaderPath );

    CleanUp( InputImages, IntermediateImages, AllTransformations, TransformationToApply );

    std::cout << "JPGConversionTest Test success" << std::endl;
}

void extraTest(){

}