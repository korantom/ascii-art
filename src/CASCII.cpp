#include "CASCII.h"

using namespace std;


CASCII::CASCII( const CPixelMatrix& matrix, size_t maxHeight, size_t maxWidth )
        : m_maxHeight( maxHeight ),
          m_maxWidth( maxWidth ),
          m_dataOriginal( matrix ),
          m_shadder( nullptr ),
          m_defaultShadder( new CShader()) {}

CASCII::CASCII( const CPixelMatrix& matrix )
        : CASCII( matrix, ASCII_HEIGHT, ASCII_WIDTH ) {}

CASCII::~CASCII() {
    delete m_defaultShadder;
}

void CASCII::ToASCII( const string& filePath, CShader *shader ) {

    ToASCIITXT( filePath, shader );
//        ToASCIIHTML( filePath, shader );
//        ToASCIIFULLRESOLUTION( filePath, shader );
}

void CASCII::ToASCIIHTML( const string& filePath, CShader *shader ) //html
{
    CPixelMatrix dataCompressed( m_dataOriginal, m_maxHeight, m_maxWidth );
    std::ofstream ofs( filePath + "_ASCII.html", std::ios::out );

    if ( shader != nullptr ) {
        delete m_shadder;
        m_shadder = nullptr;
        m_shadder = shader;
    } else if ( m_shadder == nullptr ) {
        m_shadder = m_defaultShadder;
    }

    ofs << "<pre style=\"font: 10px/5px monospace;\">";

    for ( vector <uint8_t>& row : dataCompressed.m_matrix ) {
        for ( uint8_t rowVal : row ) {
            ofs << m_shadder->Match( rowVal );
        }
        ofs << '\n';
    }

    ofs << "</pre>";

}

void CASCII::ToASCIITXT( const string& filePath, CShader *shader )//normal
{
    CPixelMatrix dataCompressed( m_dataOriginal, m_maxHeight, m_maxWidth );
    std::ofstream ofs( filePath + "_ASCII.txt", std::ios::out );

    if ( shader != nullptr ) {
        delete m_shadder;
        m_shadder = nullptr;
        m_shadder = shader;
    } else if ( m_shadder == nullptr ) {
        m_shadder = m_defaultShadder;
    }


    for ( vector <uint8_t>& row : dataCompressed.m_matrix ) {
        for ( uint8_t rowVal : row ) {
            ofs << m_shadder->Match( rowVal );
        }
        ofs << '\n';
    }

}

void CASCII::ToASCIIFULLRESOLUTION( const string& filePath, CShader *shader )//no compression
{
    std::ofstream ofs( filePath + "_ASCII.txt", std::ios::out );

    if ( shader != nullptr ) {
        delete m_shadder;
        m_shadder = nullptr;
        m_shadder = shader;
    } else if ( m_shadder == nullptr ) {
        m_shadder = m_defaultShadder;
    }


    for ( vector <uint8_t>& row : m_dataOriginal.m_matrix ) {
        for ( uint8_t rowVal : row ) {
            ofs << m_shadder->Match( rowVal );
        }
        ofs << '\n';
    }

}
