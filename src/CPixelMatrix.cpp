#include "CPixelMatrix.h"

using namespace std;

CPixelMatrix::CPixelMatrix() : m_height( 0 ), m_width( 0 ), m_name( "" ), m_matrix() {}

CPixelMatrix::CPixelMatrix( const CPixelMatrix& other ) : m_height( other.m_height ), m_width( other.m_width ), m_name( other.m_name ) {
    int i = 0;
    m_matrix.reserve( m_height );
    for ( auto& row : other.m_matrix ) {
        m_matrix.emplace_back( vector<uint8_t>());
        m_matrix[i].reserve( m_width );
        for ( uint8_t rowVal : row ) {
            m_matrix[i].emplace_back( rowVal );
        }
        i++;
    }
}

CPixelMatrix& CPixelMatrix::operator=( const CPixelMatrix& other ) {
    this->m_matrix.clear();
    m_height = other.m_height;
    m_width = other.m_width;
    m_name = other.m_name;

    int i = 0;
    m_matrix.reserve( m_height );
    for ( const vector <uint8_t>& row : other.m_matrix ) {
        m_matrix.emplace_back( vector<uint8_t>());
        m_matrix[i].reserve( m_width );
        for ( uint8_t rowVal : row ) {
            m_matrix[i].emplace_back( rowVal );
        }
        i++;
    }
    return *this;
}


CPixelMatrix::CPixelMatrix( const CPixelMatrix& other, const int& maxHeight, const int& maxWidth ) {

    int compressionCoefficient;
    int widthCoeff = (int) ceil((1.0 * other.m_width) / maxWidth ); //how many pixels i need to average to get desired width
    int heightCoeff = (int) ceil((1.0 * other.m_height) / maxHeight ); //how many pixels i need to average to get desired height
    compressionCoefficient = (widthCoeff < heightCoeff) ? heightCoeff : widthCoeff; //average by squares -> take the bigger value
    int squareAverage = 0;

    if ( other.m_width > maxWidth || other.m_height > maxHeight ) { // jsou vetsi rozmery obrazku ..je treba kpmpress

        for ( int i = 0; i + compressionCoefficient < other.m_height; i += compressionCoefficient ) {
            m_matrix.emplace_back( vector<uint8_t>());

            for ( int j = 0; j < other.m_width; j += compressionCoefficient ) {

                squareAverage = 0;
                for ( int k = i; k < i + compressionCoefficient; ++k ) {
                    if ( i >= other.m_height ) {//buffer overflow
                        break;
                    }
                    for ( int l = j; l < j + compressionCoefficient; ++l ) {
                        if ( l >= other.m_width ) {//buffer overflow
                            break;
                        }
                        squareAverage += other.m_matrix[k][l];
                    }
                }

                squareAverage /= compressionCoefficient * compressionCoefficient;
                m_matrix[i / compressionCoefficient].push_back((uint8_t) squareAverage );
            }
        }

    } else { //neni treba kompres
        for ( int i = 0; i < other.m_height; ++i ) {
            m_matrix.emplace_back( vector<uint8_t>());
            for ( int j = 0; j < other.m_width; ++j ) {
                m_matrix[i].push_back( other.m_matrix[i][j] );
            }
        }
    }
    if ( !m_matrix.empty()) {
        m_width = m_matrix[0].size();
        m_height = m_matrix.size();
    } else {
        m_width = m_height = 0;
    }

}

void CPixelMatrix::ApplyTransformations( const vector<CTransformation *>& TransformationToApply ) {
    for ( auto& t : TransformationToApply ) {
        t->Apply( *this );
    }
}

void CPixelMatrix::Clear() {
    m_matrix.clear();//sufficient? or need to itterate over..
    m_height = m_width = 0;
}


