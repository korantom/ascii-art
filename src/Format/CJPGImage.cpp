#include "CJPGImage.h"

using namespace std;

CJPGImage::CJPGImage() : m_name( "" ), m_width( 0 ), m_height( 0 ) {}

CJPGImage::CJPGImage( const string& fileName ) : m_width( 0 ), m_height( 0 ) {
    LoadImage( fileName );
}

CJPGImage::~CJPGImage() = default;

bool CJPGImage::ToPixelArray( CPixelMatrix& pixelMatrix ) const {
    if ( m_name.empty()) {
        return false;
    }
    pixelMatrix.Clear();
    pixelMatrix.m_height = m_height;
    pixelMatrix.m_width = m_width;
    pixelMatrix.m_name = this->m_name;

    pixelMatrix.m_matrix.reserve( m_height );
    for ( int i = 0; i < m_height; ++i ) {
        pixelMatrix.m_matrix.emplace_back( vector<uint8_t>());
        pixelMatrix.m_matrix[i].reserve( m_width );
    }

    int colorAverage = 0;

    for ( int i = 0; i < m_height; ++i ) {
        for ( int j = 0; j < m_width * 3; j += 3 ) {
            colorAverage = 0;
            for ( int k = 0; k < 3; ++k ) {
                colorAverage += (int) m_data[i][j + k];
            }
            colorAverage /= 3;
            pixelMatrix.m_matrix[i].push_back((uint8_t) colorAverage );
        }

    }
    return true;


}

int CJPGImage::GetHeight() const {
    return m_height;
}

int CJPGImage::GetWidth() const {
    return m_width;
}

bool CJPGImage::LoadImage( const string& filePath ) {
    if ( filePath.empty()) //todo filepath is valid nebo prenechat na ui
        return false;

    this->Clear();
    this->extractFileName( filePath, m_name, m_extension );

    uint8_t *data;
    LoadJPEG( filePath.c_str(), m_width, m_height, data );

    m_data.reserve( m_height );
    for ( int i = 0; i < m_height; ++i ) {
        m_data.emplace_back( vector<uint8_t>());
        m_data[i].reserve( m_width * 3 );
        for ( int j = 0; j < m_width * 3; ++j ) {
            m_data[i].emplace_back( data[i * m_width * 3 + j] );
        }
    }
    delete[]data;
    return true;
}

bool CJPGImage::LoadJPEG( const char *fileName, int& w, int& h, uint8_t *& data ) {
    struct jpeg_decompress_struct cinfo;
    struct my_error_mgr jerr;
    FILE *fp;

    if ((fp = fopen( fileName, "rb" )) == NULL )
        return false;
    /* Step 1: allocate and initialize JPEG decompression object */
    /* We set up the normal JPEG error routines, then override error_exit. */
    cinfo.err = jpeg_std_error( &jerr.pub );
    jerr.pub.error_exit = my_error_exit;
    /* Establish the setjmp return context for my_error_exit to use. */
    if ( setjmp( jerr.setjmp_buffer )) {
        jpeg_destroy_decompress( &cinfo );
        fclose( fp );
        return false;
    }
    /* Now we can initialize the JPEG decompression object. */
    jpeg_create_decompress( &cinfo );


    /* Step 2: specify data source (eg, a file) */
    jpeg_stdio_src( &cinfo, fp );

    /* Step 3: read file parameters with jpeg_read_header() */
    (void) jpeg_read_header( &cinfo, TRUE );


    /* Step 4: set parameters for decompression */
    /* In this example, we don't need to change any of the defaults set by
     * jpeg_read_header(), so we do nothing here.
     */
    /* Step 5: Start decompressor */
    (void) jpeg_start_decompress( &cinfo );

    if ( cinfo.output_components != 3 ) { // only RGB images supported in this example
        jpeg_destroy_decompress( &cinfo );
        fclose( fp );
        return false;
    }

    int row_stride = cinfo.output_width * cinfo.output_components;
    /* Make a one-row-high sample array that will go away when done with image */

    w = cinfo.output_width;
    h = cinfo.output_height;

    data = new uint8_t[w * h * 3];
    long counter = 0;

    // step 6, read the image line by line
    while ( cinfo.output_scanline < cinfo.output_height ) {
        JSAMPROW buffer[1] = {(JSAMPROW)( data + counter )};
        jpeg_read_scanlines( &cinfo, buffer, 1 );
        counter += row_stride;
    }
    /* Step 7: Finish decompression */

    (void) jpeg_finish_decompress( &cinfo );
    /* Step 8: Release JPEG decompression object */

    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_decompress( &cinfo );
    fclose( fp );

    return true;
}

void CJPGImage::my_error_exit( j_common_ptr cinfo ) {
    /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
    my_error_mgr *myerr = (my_error_mgr *) cinfo->err;

    /* Always display the message. */
    /* We could postpone this until after returning, if we chose. */
    (*cinfo->err->output_message)( cinfo );

    /* Return control to the setjmp point */
    longjmp( myerr->setjmp_buffer, 1 );
}

void CJPGImage::Clear() {
    for ( auto& x : m_data )
        x.clear();
    m_data.clear();
}

