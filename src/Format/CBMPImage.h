#pragma once

#include "../CPixelMatrix.h"
#include "../CShader.h"
#include "CImage.h"

#include <cstring>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------------------
//todo check how much data was read using gcount + exceptions
/** Template write function that writes data of various sizes ( ex. uint8_t, uint32_t ...).
 * @tparam[in] _UINT data type of any size,  specifies how much data will be written
 * @param[in] outputFileStream  stream in which we write the data
 * @param[in] data  data of type _UINT
 */
template<typename _UINT>
void write( std::ofstream& outputFileStream, const _UINT& data );

/** Template read function that reads data of various sizes ( ex. uint8_t, uint32_t ...).
 * @tparam[in] _UINT data type of any size, specifies how much data will be read
 * @param[in] inputFileStream stream from which we read
 * @param[out] data variable where we store the read data
 */
template<typename _UINT>
void read( std::ifstream& inputFileStream, _UINT& data );


/** BMP Image class.
 * allows reading and (manipulating) a bmp image (8, 24, 32 bit formats without compression)
 * converts image to an intermediate format (basically 8 bit bmp grayscale but without any metadata/headers)
 *
 */
class CBMPImage : public CImage {
public:

    /** BMP File header.
     * Stores basic/general info about file
     * 14 Bytes
     */
    struct BMPFileHeader {
        uint16_t m_signature{0x4D42};          ///< File type always MUST BE 'BM' == 0x4D42
        uint32_t m_file_size{0};               ///< Total size of the file in Bytes
        uint16_t m_reserved1{0};               ///< Unused, always MUST BE zero 0
        uint16_t m_reserved2{0};               ///< Unused, always MUST BE zero 0
        uint32_t m_offset_data{0};             ///< Start position of pixel data/array (actual image data)
//------------------------------------------------------------------------------------------------------------------------------------------
        /** Reads the whole File header from a file stream.
         * @param[in] inputFileStream
         */
        void Read( std::ifstream& inputFileStream ) {
            read( inputFileStream, m_signature );
            read( inputFileStream, m_file_size );
            read( inputFileStream, m_reserved1 );
            read( inputFileStream, m_reserved2 );
            read( inputFileStream, m_offset_data );

        }

        /** Writes the whole header into a file stream.
         * @param[in] outputFileStream
         */
        void Write( std::ofstream& outputFileStream ) const {
            write( outputFileStream, m_signature );
            write( outputFileStream, m_file_size );
            write( outputFileStream, m_reserved1 );
            write( outputFileStream, m_reserved2 );
            write( outputFileStream, m_offset_data );

        }
    };


    /** DIB/BMPI Header.
     * stores detailed information about the file
     * specifies format in which the data should be interpreted
     * sign of width and height specifies from which end of the pixel array to interpret data
     *      ( + -> from bottom to up, - -> from top to bottom )
     * 40 Bytes
     */
    struct BMPInfoHeader {
        uint32_t m_info_header_size{0};           ///< Size of BMPInfoHeader in Bytes
        int32_t m_width{0};                      ///< Width of bitmap in pixels, has to be signed
        int32_t m_height{0};                     ///< Height of bitmap in pixels, has to be signed
        uint16_t m_planes{1};                    ///< Number of color of planes, always MUST BE 1
        uint16_t m_bits_per_pixel{0};            ///< Number of bits per pixel, channel size = m_bits_per_pixel/Byte_size
        uint32_t m_compression{0};               ///< Compression flag, specifies what compression was used, (0/3 -> no Compression)
        uint32_t m_image_size{0};                ///< Size of image data in Bytes, padding included ( width must be divisible by 4)
        int32_t m_x_pixels_per_meter{0};         ///< Horizontal resolution of the image ?unused
        int32_t m_y_pixels_per_meter{0};         ///< Vertical resolution of the image ?unused
        uint32_t m_colors_used{0};               ///< Number of color indexes in the color table, or 0 if all are used
        uint32_t m_colors_important{0};          ///< Number of important colors used for displaying the bitmap, or 0 if all are required
//------------------------------------------------------------------------------------------------------------------------------------------
        /** Reads the whole Info header from a file stream.
        * @param[in] inputFileStream
        */
        void Read( std::ifstream& inputFileStream ) {
            read( inputFileStream, m_info_header_size );
            read( inputFileStream, m_width );
            read( inputFileStream, m_height );
            read( inputFileStream, m_planes );
            read( inputFileStream, m_bits_per_pixel );
            read( inputFileStream, m_compression );
            read( inputFileStream, m_image_size );
            read( inputFileStream, m_x_pixels_per_meter );
            read( inputFileStream, m_y_pixels_per_meter );
            read( inputFileStream, m_colors_used );
            read( inputFileStream, m_colors_important );

        }

        /**
        * Writes the whole Info header into a file stream
        * @param[in] outputFileStream
        */
        void Write( std::ofstream& outputFileStream ) const {

            write( outputFileStream, m_info_header_size );
            write( outputFileStream, m_width );
            write( outputFileStream, m_height );
            write( outputFileStream, m_planes );
            write( outputFileStream, m_bits_per_pixel );
            write( outputFileStream, m_compression );
            write( outputFileStream, m_image_size );
            write( outputFileStream, m_x_pixels_per_meter );
            write( outputFileStream, m_y_pixels_per_meter );
            write( outputFileStream, m_colors_used );
            write( outputFileStream, m_colors_important );

        }
    };

    /** Color Header.
     * used only for 32bit images
     * details: https://en.wikipedia.org/wiki/BMP_file_format
     * 20 Bytes
     */
    struct BMPColorHeader {
        uint32_t red_mask{0x00ff0000};
        uint32_t green_mask{0x0000ff00};
        uint32_t blue_mask{0x000000ff};
        uint32_t alpha_mask{0xff000000};
        uint32_t color_space_type{0x0};
        uint32_t unused[16]{0};
//------------------------------------------------------------------------------------------------------------------------------------------
        /** Reads the whole Color header from a file stream.
        * @param[in] inputFileStream
        */
        void Read( std::ifstream& inputFileStream ) {
            read( inputFileStream, red_mask );
            read( inputFileStream, green_mask );
            read( inputFileStream, blue_mask );
            read( inputFileStream, alpha_mask );
            read( inputFileStream, color_space_type );
            for ( auto& u: unused ) {
                read( inputFileStream, u );
            }
        }

        /**
        * Writes the whole Color header into a file stream
        * @param[in] outputFileStream
        */
        void Write( std::ofstream& outputFileStream ) const {
            write( outputFileStream, red_mask );
            write( outputFileStream, green_mask );
            write( outputFileStream, blue_mask );
            write( outputFileStream, alpha_mask );
            write( outputFileStream, color_space_type );
            for ( auto& u: unused ) {
                write( outputFileStream, u );
            }
        }

    };

    /** Color Table.
     * used only for 8bit images
     * details: https://en.wikipedia.org/wiki/BMP_file_format
     * Lists colors used in the image
     * 256*4 = 1024 Bytes
     */
    struct BMPColorTable {
        uint8_t colors[256][4]{{0}};  //[0,0,0,0] [1,1,1,0] [2,2,2,0] [3,3,3,0]
//------------------------------------------------------------------------------------------------------------------------------------------
        /** Reads the whole Color table from a file stream.
        * @param[in] inputFileStream
        */
        void Read( std::ifstream& inputFileStream ) {
            for ( auto& p: colors ) {
                for ( auto& c : p ) {
                    read( inputFileStream, c );
                }
            }
        }

        /** Writes the whole Color table into a file stream.
        * @param[in] outputFileStream
        */
        void Write( std::ofstream& outputFileStream ) const {
            for ( auto& p: colors ) {
                for ( auto c : p ) {
                    write( outputFileStream, c );
                }
            }
        }
    };
//------------------------------------------------------------------------------------------------------------------------------------------
    /** No param constructor.
     * initializes values to empty/zero values
     */
    CBMPImage();

    /** Constructor which receives a filePath and loads it.
     * @param[in] filePath
     */
    explicit CBMPImage( const string& filePath );

    //todo
    CBMPImage( const CBMPImage& other );

    //todo
    CBMPImage& operator=( const CBMPImage& other );

    ~CBMPImage() override;// = default;

//------------------------------------------------------------------------------------------------------------------------------------------
    /** Loads image of a bmp format and extracts file name.
     * @param[in] filePath
     * @return true if image was loade, else false (wrong path, file couldnt be opened ...)
     */
    bool LoadImage( const string& filePath ) override;


    /** Copies loaded bmp image if present.
     * @param[in] filePath
     * @return true if succes, else false(no image loaded, bad filePath ..)
     */
    bool CopyImage( const string& filePath ) const;

    /** converts image to an compressed intermediate format(8 bit grayscale), with image origin in top left corner.
     * @param[out] pixelMatrix wrapper class where the image data is writen to
     * @return true if success, else false (no image present / unsupported image was loaded )
     */
    bool ToPixelArray( CPixelMatrix& pixelMatrix ) const override;

    /** Height getter.
     * @return height in pixels
     */
    int GetHeight() const override;

    /** Width getter.
     * @return width in pixels
     */
    int GetWidth() const override;

    /** Checks if files are identical.
     * @param fileName_1
     * @param fileName_2
     * @return true if files are identical or both dont exist, else false
     */
    static bool identicalFiles( const string& fileName_1, const string& fileName_2 );


private:
    BMPFileHeader m_fileHeader;             ///< Always needed
    BMPInfoHeader m_infoHeader;             ///< Always needed
    BMPColorHeader m_colorHeader;           ///< Only for 32 bit
    BMPColorTable m_colorTable;             ///< Only for <= 8 bit
    std::vector <std::vector<uint8_t>> m_data; ///< image data, ussualy stored from bottom to top

    int m_padding;                          ///< if width*pixel_size isnt divisible by 4, number of uint8_t padding zeroes, (padding cant occure in ...)
    int m_pixelSize;                        ///< m_bits_per_pixel / 8
    string m_name;                          ///< file name not path, (indicator if file is loaded or not)
    const string m_extension = ".bmp";      //todo static
//------------------------------------------------------------------------------------------------------------------------------------------
    /** Reads all headers.
     * @param[in] inputFileStream
     * @return false if unsupported format, else true
     */
    bool ReadHeader( std::ifstream& inputFileStream );

    /** Checks header data.
     * @return false if unsupported format or wrong data, else true
     */
    bool CheckHeader() const;

    /** Reads image data into a 2d vector m_data.
     *  if padding zeroes are present, they are discarded
     * @param[in] inputFileStream
     * @return always true
     */
    bool ReadData( std::ifstream& inputFileStream );

    /** Copies headers if present.
     * @param[in] outputFileStream
     */
    void CopyHeader( std::ofstream& outputFileStream ) const;

    /** Copies image data and adds padding zeroes if needed.
     * @param outputFileStream
     * @return
     */
    bool CopyData( std::ofstream& outputFileStream ) const;

    /** Clears stored image data.
     */
    void Clear();


};

