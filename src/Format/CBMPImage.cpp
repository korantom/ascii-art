#include "CBMPImage.h"

using namespace std;

template<typename _UINT>
void write( std::ofstream& outputFileStream, const _UINT& data ) {
    outputFileStream.write((const char *) &data, sizeof( data ));
}

template<typename _UINT>
void read( std::ifstream& inputFileStream, _UINT& data ) {
    inputFileStream.read((char *) &data,
                         sizeof( data ));//if(inputFileStream.read((char *) &data, sizeof( data ) ). gcount () != sizeof ( data ) ) return false; //needed?
}

CBMPImage::CBMPImage() : m_padding( 0 ), m_pixelSize( 0 ), m_name( "" ) {}

CBMPImage::CBMPImage( const string& filePath ) : m_padding( 0 ), m_pixelSize( 0 ) {
    LoadImage( filePath );
}

CBMPImage::~CBMPImage() = default;

bool CBMPImage::LoadImage( const string& filePath ) {

    if ( filePath.empty()) //todo filepath is valid nebo prenechat na ui
        return false;

    this->Clear();
    this->extractFileName( filePath, m_name, m_extension );

    std::ifstream inputFileStream( filePath, std::ios::in | std::ios::binary );

    if ( inputFileStream.fail() || !inputFileStream )
        return false;//

    if ( ReadHeader( inputFileStream ) && CheckHeader() && ReadData( inputFileStream )) {
        inputFileStream.close();
        return true;
    }

    inputFileStream.close();
    return false;

}


bool CBMPImage::CopyImage( const string& filePath ) const {
    if ( m_name.empty())   //todo filepath is valid
        return false;

    std::ofstream outputFileStream( filePath, std::ios::out | std::ios::binary );

    if ( outputFileStream.fail() || !outputFileStream )
        return false;//

    CopyHeader( outputFileStream );
    CopyData( outputFileStream );

    outputFileStream.close();
    return true;
}


bool CBMPImage::ToPixelArray( CPixelMatrix& pixelMatrix ) const {

    if ( m_name.empty()) {
        return false;
    }

    pixelMatrix.Clear();
    pixelMatrix.m_height = m_infoHeader.m_height;
    pixelMatrix.m_width = m_infoHeader.m_width;
    pixelMatrix.m_name = this->m_name;

    //priprava abych mohl vkladat i od konce
    pixelMatrix.m_matrix.reserve( m_infoHeader.m_height );
    for ( int i = 0; i < m_infoHeader.m_height; ++i ) {
        pixelMatrix.m_matrix.emplace_back( vector<uint8_t>());
        pixelMatrix.m_matrix[i].reserve( m_infoHeader.m_width );
    }

    int z = m_infoHeader.m_height - 1;
    int colorAverage = 0;

    //this.ToGrayScale( pixelMatrix.m_matrix, m_pixelSize ); nebo pretizit dve fce?
    if ( m_pixelSize == 3 || m_pixelSize == 1 ) {

        for ( int i = m_infoHeader.m_height - 1; i >= 0; --i ) {
            for ( int j = 0; j < m_infoHeader.m_width * m_pixelSize; j += m_pixelSize ) {
                colorAverage = 0;
                for ( int k = 0; k < m_pixelSize; ++k ) {
                    colorAverage += (int) m_data[i][j + k];
                }
                colorAverage /= m_pixelSize;
                pixelMatrix.m_matrix[z - i].push_back((uint8_t) colorAverage );
            }

        }
        return true;

    } else if ( m_pixelSize == 4 ) {
        for ( int i = m_infoHeader.m_height - 1; i >= 0; --i ) {
            for ( int j = 0; j < m_infoHeader.m_width * m_pixelSize; j += m_pixelSize ) {
                colorAverage = 0;
                for ( int k = 0; k < 3; ++k ) {
                    colorAverage += (int) m_data[i][j + k];
                }
                colorAverage /= 3;
                if ( m_data[i][j + 3] < 1 )//if oppacity = 0% -> white
                    pixelMatrix.m_matrix[z - i].push_back( 255 );
                else
                    pixelMatrix.m_matrix[z - i].push_back((uint8_t) colorAverage );
            }

        }
        return true;

    } else {
        return false;
    }

}


int CBMPImage::GetHeight() const {
    return m_infoHeader.m_height;
}


int CBMPImage::GetWidth() const {
    return m_infoHeader.m_width;
}


bool CBMPImage::identicalFiles( const string& fileName_1, const string& fileName_2 ) {
    ifstream inputFileStream_1( fileName_1, ios::in | ios::binary );
    ifstream inputFileStream_2( fileName_2, ios::in | ios::binary );

    if ( !inputFileStream_1 || !inputFileStream_2 )
        return false;
    char c, d;

    while ( inputFileStream_1.get( c ) && inputFileStream_2.get( d ))  // if get() fails -> error reading file or EOF
    {
        if ( c != d ) {
            inputFileStream_1.close();
            inputFileStream_2.close();
            return false;
        }
    }


    inputFileStream_1.close(); // close files. Always. As soon as you do not need them.
    inputFileStream_2.close(); // close files. Always. As soon as you do not need them.

    return true;
}


bool CBMPImage::ReadHeader( std::ifstream& inputFileStream ) {
    m_fileHeader.Read( inputFileStream );
    m_infoHeader.Read( inputFileStream );
    if ( m_infoHeader.m_info_header_size > 40 )
        m_colorHeader.Read( inputFileStream );

    if ( m_infoHeader.m_bits_per_pixel <= 8 )
        m_colorTable.Read( inputFileStream );

    return true;
}


bool CBMPImage::CheckHeader() const {
    //todo must be
    if ( m_fileHeader.m_signature != 0x4D42 )
        return false;
    if ( m_infoHeader.m_planes != 1 )
        return false;

    //todo my rules for format
    if ( m_infoHeader.m_compression != 0 && m_infoHeader.m_compression != 3 )
        return false;
    if ( m_infoHeader.m_bits_per_pixel != 8 && m_infoHeader.m_bits_per_pixel != 24 && m_infoHeader.m_bits_per_pixel != 32 )
        return false;
    if ( m_infoHeader.m_width < 0 || m_infoHeader.m_height < 0 ) //can be fixed
        return false;

    //todo extras strict for control only remove later
    if ( m_fileHeader.m_reserved1 != 0 || m_fileHeader.m_reserved2 != 0 )
        return false;
    if ( m_infoHeader.m_colors_used != 0 || m_infoHeader.m_colors_important != 0 )
        return false;


    return true;
}


bool CBMPImage::ReadData( std::ifstream& inputFileStream ) {
    uint8_t channel = 0;
    m_pixelSize = m_infoHeader.m_bits_per_pixel / 8;

    m_padding = 4 - (m_infoHeader.m_width * (m_pixelSize)) % 4;
    m_padding = (m_padding == 4) ? 0 : m_padding;

    inputFileStream.seekg( m_fileHeader.m_offset_data );//todo * m_pixelSize?

    m_data.reserve( m_infoHeader.m_height );
    for ( int i = 0; i < m_infoHeader.m_height; ++i ) {
        m_data.emplace_back( std::vector<uint8_t>());
        m_data[i].reserve( m_infoHeader.m_width );
        //data
        for ( int j = 0; j < m_infoHeader.m_width * m_pixelSize; ++j ) {
            read( inputFileStream, channel ); //inputFileStream.read((char *) &channel, sizeof( channel ));
            m_data[i].push_back( channel );// brga; blue green red alpha
        }
        //padded zeroes
        for ( int k = 0; k < m_padding; ++k ) {
            read( inputFileStream, channel );// inputFileStream.read((char *) &channel, sizeof( channel ));
        }
    }
    return true;
}


void CBMPImage::CopyHeader( std::ofstream& outputFileStream ) const {
    m_fileHeader.Write( outputFileStream );
    m_infoHeader.Write( outputFileStream );
    if ( m_infoHeader.m_info_header_size > 40 )
        m_colorHeader.Write( outputFileStream );

    if ( m_infoHeader.m_bits_per_pixel == 8 )
        m_colorTable.Write( outputFileStream );

}


bool CBMPImage::CopyData( std::ofstream& outputFileStream ) const {
    uint8_t channel = 0;
    //needed?
    while ( outputFileStream.tellp() < m_fileHeader.m_offset_data ) {
        outputFileStream.write((const char *) &channel, sizeof( channel ));
    }

    for ( int i = 0; i < m_infoHeader.m_height; ++i ) {
        //data
        for ( int j = 0; j < m_infoHeader.m_width * m_pixelSize; ++j ) {
            outputFileStream.write((const char *) &m_data[i][j], sizeof( m_data[i][j] ));
        }
        //padding zeroes
        for ( int k = 0; k < m_padding; ++k ) {
            outputFileStream.write((const char *) &channel, sizeof( channel ));
        }
    }
    return true;
}


void CBMPImage::Clear() {
    for ( auto& x : m_data )
        x.clear();
    m_data.clear();
}


