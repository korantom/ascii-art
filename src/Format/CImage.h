#pragma once

#include "../CPixelMatrix.h"
#include <string>

//------------------------------------------------------------------------------------------------------------------------------------------

/** Abstract Parent class for all Image formats (BMP, JPG, PNG ...).
 * Provides/Ensures basic interface for user:
 *      loads Images
 *      gets width/height
 *      converts to an intermediate format(CPixelMatrix)
 */
class CImage {
public:
    virtual ~CImage() = default;

    /** Converts image to a 8bit pixel array.
     *
     * looses all unneccesary information, data transformed into raw/simple 8bit grayscale 2d array
     *
     * @param[out] pixelMatrix wrapper class where we write the images compressed data
     * @return  true if image was coverted successfully, else false
     */
    virtual bool ToPixelArray( CPixelMatrix& pixelMatrix ) const = 0;

    virtual int GetHeight() const = 0;

    virtual int GetWidth() const = 0;

    //    virtual string GetFileName() = 0;

    /** Loads image of a given format from given path.
     * @param[in] filePath
     * @return true if image was loaded successfully, else false
     */
    virtual bool LoadImage( const string& filePath ) = 0;

    //todo
    //static bool identicalFiles( const string& fileName_1, const string& fileName_2 );

protected:
    //    string m_fileName;
    const static char m_fileSystemSeparator = '/'; ///< Linux file system separator
//------------------------------------------------------------------------------------------------------------------------------------------
    /** Extracts file name from file path.
     * cuts of path and extension leaving only the file name
     * @param filePath[in]  file path
     * @param fileName[out] variable where we write the extracted name
     * @param extension[in] file extension (ex. .jpg or .bmp)
     */
    inline static void extractFileName( const string& filePath, string& fileName, const string& extension = "" ) {
        fileName = filePath;
        int nameBeginPos = 0;
        for ( int i = filePath.length() - 1; i >= 0; --i ) {
            nameBeginPos = i;
            if ( filePath[i] == m_fileSystemSeparator )
                break;
        }
        if ( nameBeginPos == 0 ) {
            fileName = filePath.substr( 0, filePath.length() - extension.length());
        } else {
            fileName = filePath.substr( nameBeginPos + 1, filePath.length() - (nameBeginPos + 1 + extension.length()));
        }
    }

};
