#pragma once
//my
#include "../CPixelMatrix.h"
#include "../CShader.h"
#include "CImage.h"
//c++
#include <cstring>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

#include <jpeglib.h>
#include <csetjmp>
#include <stdint.h>

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------------

/** JPG Image class.
 * allows reading/decompresing and (manipulating) a JPG
 * converts image to an intermediate format (basically 8 bit bmp grayscale but without any metadata/headers)
 */
class CJPGImage : public CImage {
public:
    /** No param constructor.
     * initializes values to empty/zero values
     */
    CJPGImage();

    /**
     * Constructor which receives a filePath and loads it
     * @param[in] filePath
     */
    explicit CJPGImage( const string& fileName );

    //todo
    CJPGImage( const CJPGImage& other );

    //todo
    CJPGImage& operator=( const CJPGImage& other );

    ~CJPGImage() override;

//------------------------------------------------------------------------------------------------------------------------------------------

    /** converts image to an compressed intermediate format(8 bit grayscale), with image origin in top left corner.
     * @param[out] pixelMatrix wrapper class where the image data is writen to
     * @return true if success, else false (no image present / unsupported image was loaded )
     */
    bool ToPixelArray( CPixelMatrix& pixelMatrix ) const override;

    /** Height getter.
     * @return height in pixels
     */
    int GetHeight() const override;

    /** Width getter.
     * @return width in pixels
     */
    int GetWidth() const override;

    /** Loads image of a jpg format.
     * uses LoadJPEG method
     * @param[in] filePath
     * @return true if image was loade, else false (wrong path, file couldnt be opened ...)
     */
    bool LoadImage( const string& filePath ) override;


private:
    string m_name;
    int m_width;
    int m_height;
    std::vector <std::vector<uint8_t>> m_data;
    const string m_extension = ".jpg";
//------------------------------------------------------------------------------------------------------------------------------------------

    struct my_error_mgr {
        struct jpeg_error_mgr pub;    /* "public" fields */
        jmp_buf setjmp_buffer;    /* for return to caller */
    };

    static void my_error_exit( j_common_ptr cinfo );

    /** Decompresses and Loads jpg image into an unsigned char 1D array.
     *  This methods code is based on code from class.
     * @param[in] fileName path to image
     * @param[out] w variable where the image width is stored
     * @param[out] h variable where the image height is stored
     * @param[out] data reference to an pointer to unisigned char
     * @return true if succes else false
     */
    bool LoadJPEG( const char *fileName, int& w, int& h, uint8_t *& data );

    /** Clears stored image data.
     */
    void Clear();
};

