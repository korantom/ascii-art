#pragma once

#include "CPixelMatrix.h"

#include <cstring>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

using namespace std;
//------------------------------------------------------------------------------------------------------------------------------------------

/** Shader class converts values to ascii characters.
 * provides a preset default option or user can load their own shader from a txt file with format: "ascii_char;separator;value"
 *
 */
class CShader {
public:

    /** No param constructor.
     * creates a default simple shader
     */
    CShader();

    /** Constructor which loads the shader from a given path if exists else creates a default shader.
     * forbidden char '%' problems when printing in ncurses
     * txt files has to be in "ascii_char;separator;value"
     *
     * @param[in] filePath path to txt file
     */
    explicit CShader( const string& filePath );

    /** Copy constructor.
     *
     * @param[in] other
     */
    CShader( const CShader& other );

    /** = opearator.
     * @param other
     * @return
     */
    CShader& operator=( const CShader& other );
//------------------------------------------------------------------------------------------------------------------------------------------
    /** TODO....
     * will print a NxN square showing the linear gradient from black to white of characters
     * @param str
     */
    void ExampleSmall( string& str );

    /** TODO....
     * will print a 255x255 square showing the linear gradient from black to white of characters
     * @param str
     */
    void ExampleBig( string& str );

    /** Matches a value to a char.
     *
     * @param[in] x grayscale value
     * @return char best representing (closest) to a given grayscale value
     */
    char Match( const uint8_t& x ) const;

private:
    vector <pair<char, uint8_t>> pixelToCharArray; ///< vector of pairs, char and its grayscale value
    size_t m_size;                                  ///< number of pairs

//------------------------------------------------------------------------------------------------------------------------------------------

    /** Comparator for vector of pairs<char, uint8_t> used for sorting in Ascending order.
     * @param[in] a
     * @param[in] b
     * @return a < b true, else false
     */
    static bool sortByValue( pair<char, uint8_t>& a, pair<char, uint8_t>& b );
};


